---
layout: page
title: Projets
subtitle: Mes projets
comments: false
---

* [Script Post-install pour elementary OS](https://github.com/Devil505/elementaryos-postinstall)
* [Script Post-install pour Solus](https://github.com/Devil505/solus-postinstall)
* [Dépôt perso pour Solus](https://github.com/Devil505/solus-3rd-party-repo)

