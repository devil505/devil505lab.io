---
layout: page
title: À Propos
subtitle: 
---

Utilisateur de GNU/Linux

### Utilisateur elementary OS et Solus

 * Co-gérant du site de la communauté francophone, [http://elementaryos-fr.org](http://elementaryos-fr.org)

### Ancien utilisateur et développeur pour Frugalware Linux

 * Mainteneur de Paquets
 * Membre équipe Artwork
 * Mainteneur du Planet
 * Co-mainteneur XFCE
 * Mainteneur du Magazine
