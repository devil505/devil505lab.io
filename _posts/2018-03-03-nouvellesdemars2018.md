---
title : "Nouvelles de Mars 2018"
tags : ["linux"]
categories : ["linux"]
---


## Docteur Jekyll et Mr Hugo

Avec Hugo, pour une raison que je n'ai pas réussi à déterminer, les commentaires de Disqus ne s'affichaient plus. J'ai fini par abandonner Hugo pour Jekyll, avec le thème BeautifulJekyll similaire à BeautifulHugo, on y voit que du feu...et les commentaires refonctionnent, alors n'hésitez pas à vous lâcher. :-D

## Ca peaufine chez elementary OS

On aura beau dire ce qu'on veut d'elementary OS, faut dire qu'ils savent designer leurs système et outils comme on peut le voir sur [ce billet](https://medium.com/elementaryos/juno-progress-for-january-february-9b276042716e).

## Et pendant ce temps chez Solus...

La version 4 se fait attendre et on l'espère pour ce mois-ci. Des captures du prochain Software Center sont apparues:

<img src="https://lh3.googleusercontent.com/-9z1hjJ-pnh0/WphUX1gQHOI/AAAAAAAANm8/rV8yWvoxgoAhurnNZ8GqfmVYQWoEzQS-QCJoC/w530-h298-n/Screenshot%2Bfrom%2B2018-03-01%2B19-26-20.png">

<img src="https://lh3.googleusercontent.com/-ChvCCKoZotE/WpgkWITUC3I/AAAAAAAANl4/KKCvcuOqCos5hBiegqH3lX5dAlZtVMopQCJoC/w530-h298-n/Screenshot%2Bfrom%2B2018-03-01%2B16-02-54.png">

Et Wayland a été activé à titre expérimental pour Gnome-shell:

<img src="https://lh3.googleusercontent.com/-g0VZ9ZkTDeE/WpjhbrNQ62I/AAAAAAAAGw8/e6SKC8dbv5sYGn8-GHAIZJ0tSjygq31cwCJoC/w530-h298-n/Screenshot%2Bfrom%2B2018-03-02%2B07-28-37.png">

On se retrouve en Avril !


