---
title : "Nouvelles d'Août 2018"
tags : ["linux"]
categories : ["linux"]
---

J'ai pu avoir accès à un lien de téléchargement de l'image iso de Solus avec plasma. Après quelques tests avant avec virtualbox, je l'ai finalement installé sur le laptop xiaomi. J'ai été séduit par cet environnement de bureau.

## Même avec Ikey en vacances, Solus avance !

Ce n'est pas parceque le fondateur de Solus (et Budgie) prend des vacances que le développement de la distribution est au ralenti. Les autres développeurs maintiennent la distro rolling avec la mise à jour hebdomadaire (généralement le vendredi) qui inclue des mise à jour de paquets (comme le driver NVIDIA en 390.77) mais aussi de nouvelle version du kernel (avec reconstruction des modules). Vous pouvez retrouvez les derniers paquets construits sur ce [lien](https://build.solus-project.com/).

## et aussi Budgie

Ikey n'est pas le seul développeur de Budgie, Josh l'est également. Il a conçu dans les paramètres de Budgie, une section pour le panneau Raven:

<iframe src="https://mastodon.cloud/@JoshStrobl/100471814123499093/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://mastodon.cloud/embed.js" async="async"></script>

Il sera aussi possible de monter le son au dessus des 100%:

<iframe src="https://mastodon.cloud/@JoshStrobl/100472832632180008/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://mastodon.cloud/embed.js" async="async"></script>

Enfin, un contributeur a écrit une applet Caffeine pour Budgie:

<iframe src="https://mastodon.cloud/@JoshStrobl/100473457708285282/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://mastodon.cloud/embed.js" async="async"></script>

Tout ces petites nouveautés seront dans la prochaine mise à jour mineure de Budgie.

## Des news de mon dépôt pour Solus

Comme j'ai pas trop de news pour ce mois-ci. Je vais vous parler un peu des dernières nouveautés de mon dépôt pour Solus qui se trouve désormais sur [Gitlab](https://gitlab.com/devil505/solus-3rd-party-repo).

Il y a de nouveaux paquets originellement prévus pour elementary OS (mais que j'ai packagé pour Solus) comme aesop (lecteur PDF), news (lecteur de news), monilet (moniteur système), palaura (dictionnaire) ou encore pwned-checker (pour vérifier si votre email ou mot de passe ont été piratés) mais ce dernier sera bientôt sur le dépôt officiel.

Les utilisateurs de Protonmail et ProtonVPN seront content de voir que j'ai packagé protonmail-bridge-bin (paquet fait depuis le paquet debian du bridge de protonmail et pour utiliser son client mail avec Protonmail) et protonvpn-cli (pour se connecter à un VPN de protonvpn en ligne de commande).

Les fans de la chaîne Arte ne sont pas délaissés car j'ai packagé Qarte, le logiciel connu des ubunteros francophones et qui permet de télécharger les documentaires arte+7 et arteLiveWeb.

Si vous avez un soucis avec mon dépôt, n'hésitez pas à ouvrir un ticket sur le gitlab ;-)

## Mes scripts pour Solus !

Petit bonus, [mon dépôt de scripts pour Solus sur gitlab](https://gitlab.com/devil505/scripts). Il y a un peu de tout, DNS OpenNIC et FDN, la liste noire Adzhosts pour bloquer les pubs, installation de certains logiciels non (encore?) présents sur Solus...etc. Si vous avez des suggestions, là aussi n'hésitez pas à ouvrir un ticket.

## Un peu de tristesse et de nostalgie

Et oui, c'est avec regret que j'ai appris la fin de [Quebecos](http://quebecos.com/?p=6593). J'ai connu ce site quebecois de news linuxiennes lors des mes débuts sous linux, à l'époque j'étais sur PCLinuxOS, je me souviens d'y avoir rencontrer des utilisateurs dont j'ai perdu la trace comme tyrry, Gerinald, fmo... sacrée époque.

Rendez-vous en septembre !
