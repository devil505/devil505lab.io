---
title : "Dégooglisons phase 2"
date : "2017-12-31T11:17:00+01:00"
draft : false
thumbnail : "img/edito.jpg"
toc : true # Optional
tags : ["editos"]
categories : ["editos"]
---

Dégooglisons phase 3

Je poursuis tant bien que mal ma désintoxication de google. Un service bien pratique, c'est le Google Agenda pour y mettre ses horaires, rendez-vous, rappel d'évènements, anniversaires...etc

J'ai trouvé la solution: Framagenda

J'ai crée un compte et j'ai réussi à importer mes calendriers de Google Agenda. Un point qui m’intéresse c'est la synchro avec mon smartphone. Pour ca c'est simple, il faut l'application [DAVdroid](https://f-droid.org/packages/at.bitfire.davdroid/), elle est payante sur Google Play mais gratuite via [F-droid](https://f-droid.org/). Vous installez l'apk de F-droid, puis cherchez DAVdroid pour l'installer. Rajouter le framagenda avec les paramètres qui vont bien dans DAVdroid et la synchronisation se fera.

Coté PC, on peut très bien utiliser l'interface web de Framagenda mais sachez que l'application agenda de Gnome avec Solus peut l'utiliser. Et oui, framagenda est une instance NextCloud qui est supporté par Gnome Online Account. 

Conclusion, transition réussie :-)
