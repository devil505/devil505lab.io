---
title : "[Alors on regarde ?] The Terror"
tags : ["séries TV"]
categories : ["séries TV"]
---

The Terror c'est la nouvelle série d'AMC, la chaîne qui diffuse _The Walking Dead_. La série, prduite par Ridley Scott (je sais ce n'est pas toujours une référence) est basée sur le livre de Dan Simmons du même nom qui s'inspire de l'histoire réele de l'expedition Franklin en 1847 par deux navires, le Terror et l'Erebus, qui devaient traverser l'Arctique mais qui ont finalement disparus. Simmons s'est donc servi de cette histoire pour écrire un roman fantastique. Dans la série, on retrouve dès le départ bien que lent cette atmosphère inquiètante. De plus, le sort des deux navires bloqués dans la glace autour d'un paysage blanc et vide n'augurent rien de bon. Sans trop en dévoiler, des problèmes vont survenir à l'équipage et cela va monter crescendo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rnN7Aad3c7A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Côté acteurs, y'a du lourd, ce sont des acteurs anglais connus, Jared Harris (Georges VI dans _The Crown_) joue le rôle du Capitaine (du Terror) Francis Crozier, Tobias Menzies (Franck dand _Outlander_) joue le rôle du Capitaine (de l'Erebus) James Fitzjames et Claran Hinds (_Games of Thrones_) qui joue le rôle du chef d'expédition, Sir Jhon Franklin. Il y a aussi d'autres personnages intéressants comme le Docteur Goodsir ou Mister Hickey.

<iframe width="560" height="315" src="https://www.youtube.com/embed/l328p5sSEmc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

La série n'a qu'une saison de 10 épisodes. Les décors et effets spéciaux sont vraiment crédibles. Il ne me reste que deux et je peux vous dire que cela tient en haleine même si on sait que cela va mal finir. Conclusion, on regarde !
