---
title : "Marvel's The Punisher"
date : "2018-01-02T07:53:00+02:00"
draft : false
thumbnail : "img/seriestv.jpg"
toc : true # Optional
tags : ["séries TV"]
categories : ["séries TV"]
---

<img src="http://de.web.img1.acsta.net/pictures/17/07/18/12/32/313669.jpg">

Après Defenders, netflix rebalance une nouvelle série Marvel avec un personnage issue de la saison 2 de Daredevil, le Punisher. Dans la gamme des héros Marvel, le punisher c'est quand même du lourd. C'est un ancien militaire ayant perdu sa famille qui décide de faire justice lui même car il ne croit plus au système. C'est un anti héros bien violent, un peu comme Deadpool mais en moins drôle.

<iframe width="560" height="315" src="https://www.youtube.com/embed/lIY6zFL95hE" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>

On retrouve, Frank Castle, le punisher, qui finalise sa vendetta après les événements de Daredevil, puis se fait discret sous une fausse identité. Un hacker va le contacter au sujet d'une opération où Castle avait fait le sale boulot pour un barbouze de la CIA. De plus, une nouvelle recrue du Homeland Security enquête sue la même chose. Bien évidemment, les barbouzes vont tout faire pour étouffer l'affaire.

Jon Bernthal interprète toujours aussi bien le personnage dont l'esprit est toujours aussi torturé par l'assassinat de sa femme et de ses enfants. On sent qu'il n'a rien à perdre et on retrouve le côté bourrin qui fait tout le personnage. Bon point également pour Micro, le hacker, qui fait une bonne équipe avec le punisher même s'il s'entendent comme un vieux couple.

<img src="https://pop-wrapped.s3-us-west-1.amazonaws.com/articles/93398/-the-punisher-review--1-med.jpg">

Au final, le scénario reste classique mais j'ai quand même passé un bon moment. Certaines scènes peuvent être violentes mais en même temps cela fait partie de l'univers du personnage.


<iframe width="560" height="315" src="https://www.youtube.com/embed/aOgDyEeQDUM" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
