---
title : "Ma sélection d'applications f-droid pour Septembre 2018"
tags : ["android"]
categories : ["android"]
---

Voici une petite sélection d'applications que vous trouverez sur le store de [f-droid](https://f-droid.org/).

## tutanota

Si vous avez opté pour un email qui respecte la vie privée (sinon faites-le !), vous devez connaître tutanota. Tutanota propose son application sur le Google Play store mais depuis peu, l'équipe a retravaillé son application pour que celle-ci n'ai plus besoin de passer par les services Google (par exemple la notification push nécéssite Google Cloud Messaging). Maintenant l'application tutanota dégooglisé est [disponible sur f-droid](https://f-droid.org/en/packages/de.tutao.tutanota), ProtonMail devrait normalement suivre le pas.

## briar

Le [projet Briar](https://briarproject.org/) est un projet de messagerie sécurisée plutôt destinée aux activistes et aux journalistes. Il passe par le wifi, bluetooth et le réseau tor et non via des serveurs centralisés. Sur le papier cela a l'air plutôt pas mal après faut encore trouver des gens qui l'utilise.

## offi

Avec [Offi](https://f-droid.org/packages/de.schildbach.oeffi/), vous pourrez plannifier trajet de trains et métros dans plusieurs grandes villes avec aussi la possibilité de consulter les plans. La base de données s'étoffe, attention certains plans sont encore en alpha comme celui pour Paris.

## serieguide

Via le dépôt d'izzy, je suis tombé sur [seriesguide](https://apt.izzysoft.de/fdroid/index/apk/com.battlelancer.seriesguide), appli pour suivre séries tv et films. On peut même synchroniser son compte trak.tv. Le développeur a l'air de prendre en compte le respect de la vie privée de ses sutilisateurs d'après le [site](https://seriesgui.de/privacy), il laisse l'option permettant de désactiver Google Analytics.

## weather widget

C'est le [widget météo](https://f-droid.org/en/packages/nl.implode.weer/) que j'utilise désormais, il permet d'afficher la météo sur plusieurs jours.

## p2play

Une fois n'est pas coutume, voici une application que vous ne trouverez pas (encore ?) sur F-droid car elle est en pour le moment en développement. [P2play](https://gitlab.com/agosto182/p2play/tags) est une application pour visionner les vidéos d'une instance peertube (l'aternative libre à Youtube).

<div id="commento"></div>
<script src="https://cdn.commento.io/js/commento.js"></script>