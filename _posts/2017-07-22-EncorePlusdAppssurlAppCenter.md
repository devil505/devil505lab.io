---
title : "Encore plus d'apps sur l'AppCenter"
date : "2017-07-22T07:36:00+02:00"
draft : false
thumbnail : "img/elementary.jpg"
toc : true # Optional
tags : ["linux","elementary OS"]
categories : ["elementary OS"]
---

Castrée (comme dit FredBezies) ou pas, la elementary OS a le vent en poupe chez les développeurs d'applications tierces. En effet, il y a maintenant régulièrement de nouvelles applications sur le store, pardon, l'AppCenter. 

Actuellement j'ai repéré:

* URM Simulator
* ImageBurner
* metronome

<img src="../../img/appselementary.jpeg">

L'idée de simplifier le processus de publication d'application tierces sur l'AppCenter avec également un système de rémunération sous forme de dons semble être une bonne idée.

Quelles seront les prochaines applications *elementary curated* ? Moi je mise sur [Odysseus](https://github.com/alcinnz/Odysseus). 
