---
title : "Nouvelles Mars 2017"
date : "2017-03-26T19:32:45+01:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux"]
categories : ["linux"]
---

## Alors quoi de neuf ?

#### Chez solus...

![Mate 1.18 sur Solus](../../img/desktop-solus-mate.jpeg)

Pour Solus version Mate, sachez que Mate 1.18 est disponible, il n'y a juste qu'à mettre à jour. La grosse news du moment c'est la mise en place de [clr-boot-manager par défaut](https://solus-project.com/2017/03/26/clr-boot-manager-now-available-in-solus/) pour la gestion du boot. Pour les détails voir [ce lien](https://github.com/ikeydoherty/clr-boot-manager/releases/tag/v1.5.0). Maintenant que c'est fait, Ikey va pouvoir se reconcentrer sur Budgie en version QT.

#### Chez elementary

La campagne indiegogo a été un franc succès avec 28% de dons en plus de l'objectif initial. Plus de 10 000$ en 30 jours, plutôt pas mal et de quoi taire les mauvaises langues.

<iframe width="560" height="315" src="https://www.youtube.com/embed/o0QxZAcu4d0" frameborder="0" allowfullscreen></iframe>

Du coup, cela va permettre d'ouvrir l'AppCenter à des développeurs tiers pour que ces derniers proposent leurs propres applications (et même être rémunérés), un peu comme un App Store ou Play Store.

![Daniel Foré s'amuse avec l'AppCenter](https://lh3.googleusercontent.com/-f3vNNNLIWsI/WNIIcAKIXfI/AAAAAAAAMIY/sgcIDXHzoTMFnhe5YkmiYAQHV7u2bomowCJoC/w530-h393-p-rw/Screenshot%2Bfrom%2B2017-03-21%2B23.14.12.png)

![Encore là aussi](https://lh3.googleusercontent.com/-Fxur1SPev2Q/WNAzPJvj_aI/AAAAAAAAMHc/6TiTJB-pQZYkHcNjbkB8QGjzM1cYV-e0gCJoC/w530-h333-p-rw/Screenshot%2Bfrom%2B2017-03-20%2B13.52.53.png)

On verra si cela permettra de faire avancer le schmilblick...
