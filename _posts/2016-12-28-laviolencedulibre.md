---
title : "La violence du libre"
date : "2016-12-28T11:57:54+01:00"
draft : false
thumbnail : "img/edito.jpg"
toc : true # Optional
tags : ["coup de gueule"]
categories : ["editos"]
---

L'autre soir, j'ai été un peu choqué par çà:

<iframe width="560" height="315" src="https://www.youtube.com/embed/oXZ6179GsSE" frameborder="0" allowfullscreen></iframe>

Bon alors OK je ne suis pas un grand fan des tests de Fred mais c'est le jeu, et puis c'est les goûts et les couleurs. Regardez, elementary OS se fait *défoncer* lors de chaque test tout comme à une époque la Frugalware (et encore je ne parle pas de cyrille). Mais de là à aller insulter par SMS directement Fred, cela m'a un peu choqué. J'ai parfois l'impression que dans la blogosphère du libre la polémique part très vite et dérape trop facilement.

On peut voir sur le [blog de fred](http://frederic.bezies.free.fr/blog/?p=15543#comments) que certains commentaires remettent en cause sa manière de faire ses tests et son objectivité. Il y a quelques années, fred avait testé Gentoo sur une machine virtuelle et cela avait été blasphématoire par les utilisateurs de de la distribution. Je ne suis pas là pour dire si Fred devrait arrêter ou pas ses tests, je tenais à partager mon coup de gueule pour signaler qu'en arriver à de telles polémiques et bagarres est vraiment regrettable...
