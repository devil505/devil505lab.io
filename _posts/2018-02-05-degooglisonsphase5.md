---
title : "Dégooglisons phase 5"
date : "2018-02-05T18:20:00+01:00"
draft : false
thumbnail : "img/edito.jpg"
toc : true # Optional
tags : ["editos"]
categories : ["editos"]
---

Je poursuis ma degooglisation avec cette fois pour sujet, le cloud. La cible est Google Drive où je dispose de 15 Go. J'ai également un compte dropbox pour des broutilles et un compte Méga pour de la documentation car ils offrent 50 Go gratuitement. Ces deux comptes sont vraiment des clouds secondaires.

Google Drive étant mon cloud principal, j'ai commencé par chercher une alternative du côté de framasoft avec framadrive, pas de bol, inscriptions fermées car le nombre maximal d'inscrits a été atteint.

Dernièrement, j'ai vu une news parlant de Cozy et j'ai finalement tenter ma chance. [Cozy](https://cozy.io/fr/) se présente comme un cloud qui utilise des logiciels libres et qui respecte la vie privée. Cozy est avant tout un cloud, donc un espace de stockage en ligne, de 5go gratuitement de base. Il y a une application Android et même pour linux (AppImage). En plus, c'est un projet français.

Cozy a d'autres fonction, comme Cozy Photos, qui peut stocker les photos de vos appareils et les synchroniser. Cela peut être une alternative à Google Photos au passage. Il y a aussi Cozy Collect qui permet de collecter et garder au même endroit (votre cloud de Cozy) vos factures téléphone, internet, relevés de banque, feuilles d'assurance santé... Etc. Je ne suis pas sûr d'utiliser ce service mais peut être que cela peut être utile pour d'autres. Il y a Cozy Banks en version beta qui est destiné à permettre de gérer son budget. Je n'ai pas testé mais pourquoi pas...L'équipe prépare Cozy Store qui permettra d'installer des applications dans son Cozy d'après la description.

Bon bah voilà, on peut donc se passer de Google Drive ou de d'autres clouds proprio.
