---
title : "Budgie passe à Qt"
date : "2017-01-28T08:14:50+01:00"
draft : false
thumbnail : "img/solus.jpg"
toc : true # Optional
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---

La nouvelle a ébranlé le (petit) monde de Solus cette semaine, et oui, Budgie Desktop abandonne GTK pour Qt !

Comme [l'explique Ikey sur le blog de Budgie Desktop](https://budgie-desktop.org/2017/01/25/kicking-off-budgie-11/), le problème vient de GNOME. En effet, à quasiment chaque mise à jour de version de GNOME, les développeurs changent un tas de choses qui se répercutent sur des projets tiers comme Budgie Desktop. Apparemment, la dernière mise à jour a crée des problèmes dans le développement de Budgie 11, et comme cela est récurrent, on peut comprendre l'agacement que cela peut causer. Chez elementary OS, le bureau pantheon a aussi des dépendances avec GNOME (par exemple Mutter), on peut comprendre pourquoi la distribution est basée sur une version LTS d'Ubuntu.

Ikey s'est donc tourné vers la librairie utilisé par KDE: Qt. Perso, j'ai toujours eu une préférence pour GTK que je trouve visuellement plus jolie coté thème graphique, question de goût hein. De plus, un tas de mes applications préférées sont des applications développées en GTK. Bref, je fais partie des sceptiques (mais peut-être pas pour longtemps).

Forcé de constater que Ikey est quelqu'un plein de ressources, comme on peut le voir sur Google+, il s'est déjà mis au travail pour la transition GTK->Qt:

<img src="https://lh3.googleusercontent.com/-zb_I9abDHAc/WIrro3ZHfxI/AAAAAAAAibs/PovzdMGGMv4dx5v3eCpflBk2MJxqc6khgCJoC/w530-h298-p-rw/Screenshot%2Bfrom%2B2017-01-27%2B06-40-53.png">

Le panel du bas ici utilise Qt:

<img src="https://lh3.googleusercontent.com/-m1VEDJqXwGg/WIrntd7DvNI/AAAAAAAAibI/xumWnCGqjNIAY0yGyfMelpAiElnAiM6AQCJoC/w530-h298-p-rw/Screenshot%2Bfrom%2B2017-01-27%2B06-24-20.png">

Bref le développement suit son cours:

<img src="https://lh3.googleusercontent.com/-feiP7CftHmg/WIwTYg39ZNI/AAAAAAAAidU/icPPYhNMdyMB2yWelvku38Rt6Htrxms_ACJoC/w530-h298-p-rw/Screenshot%2Bfrom%2B2017-01-28%2B03-42-58.png">

<img src="https://lh3.googleusercontent.com/-3fZ_dtEDhHc/WIwkysn1U9I/AAAAAAAAieE/rrRIKijSxn86_dHQXZ-lLJHlvmr8I2RKgCJoC/w530-h298-p-rw/Screenshot%2Bfrom%2B2017-01-28%2B04-57-11.png">

<img src="https://lh3.googleusercontent.com/-SOkgadm6xtQ/WIw-KckvSnI/AAAAAAAAie8/4EzdQDGmZ4kt7LxLSk-9mesxZtapE3kGQCJoC/w530-h298-p-rw/Screenshot%2Bfrom%2B2017-01-28%2B06-45-11.png">

J'ai envie de dire: Wait and see. Budgie Desktop risque peut être de nous surprendre dans les prochains mois :-)
