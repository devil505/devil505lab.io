---
title : "Nouvelles d'août 2017"
date : "2017-08-31T17:51:00+02:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux"]
categories : ["linux"]
---

Alors quoi de neuf durant le mois d'août pour les deux distributions Linux que je suis de près.

## Encore et encore plus d'apps sur l'AppCenter

Faut croire que la mayonnaise prend. Vite fait, j'ai vu qu'il y avait donc de nouvelles applications elementary sur l'AppCenter, hashit pour obtenir la somme sha256 d'un fichier par exemple, formatter pour formater des partitions ou périphériques externes, MuseIC qui est un lecteur de musiques (si vous n'aimez pas pantheon-music...) ou encore webpin qui crée des raccoucirs avec icones vers une adresse de site web que vous désirez.

## L'installateur se refait une beauté

Sur github, j'ai vu un tas de commits sur le dépôt de Panthéon-installer. Daniel fore a posté le résultat [sur Google+](https://plus.google.com/b/109348840800096254191/+DanielFor%C3%A9/posts/eKF2gQhK5ui) et c'est plutôt pas mal.

## Solus 3eme du nom

Solus a sorti une nouvelle snapshot et ils l'ont baptisé Solus 3, je sais pas d'où sort ce numéro m'enfin bon.. C'est un bon cru cela dit, même fredbezies est d'accord.

<iframe width="560" height="315" src="https://www.youtube.com/embed/LSWvDOpQPfY" frameborder="0" allowfullscreen></iframe>

## Budgie en qt

Ikey s'est enfin remis au développement de Budgie avec Qt. Il a posté sur [patreon](https://www.patreon.com/posts/14105902) et [sur Google+](https://plus.google.com/b/109348840800096254191/+IkeyDoherty/posts/epFeP1jVrra), ça s'annonce bien. Ma seule inquiétude c'est la fenêtre avec kwin, j'ai toujours apprécié les thèmes gtk mais on verra bien. En plus, je suis tombé sur des maquettes sympas [ici](https://github.com/budgie-desktop/budgie-desktop/issues/763].

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ot5SgCYRtS0" frameborder="0" allowfullscreen></iframe>

## Station X

Les deux distros se lancent dans l'OEM et sont fournis en version pré-installées sur les machines (un peu chères) vendues par [StationX](https://stationx.rocks/ ). FredBezies et Linuxtricks en ont déjà parlé. Perso, je ne sais pas trop quoi en penser.


## Petit bonus

Ikey doherty ne délaisse pas Brisk menu pour les utilisateurs de Mate. Il devrait bientôt supporter les clics droit avec menu contextuel sur les raccourcis si j'ai bien compris la vidéo postée:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Cseji-7vV1o" frameborder="0" allowfullscreen></iframe>

On se retrouve le mois prochain !


