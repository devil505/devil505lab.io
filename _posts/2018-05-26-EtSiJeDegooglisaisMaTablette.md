---
title : "Et si je dégooglisais ma tablette ?"
tags : ["editos"]
categories : ["editos"]
---

Après avoir trouvé des alternatives aux services de Google and co, je me suis laissé tenter par une dégooglisation de ma tablette LG G Pad 8.4 (v500).

<img src="../../img/lineageosmicrog.jpeg">

## Quel rom ?

Bien sûr, il faut changer de roms et cela fait un moment que j'avais dévérouillé le secure boot de ma tablette pour tester diffèrentes ROMs présentées sur XDA, donc là pas de réel soucis. Certains diront qu'il faut opter pour LineageOS, oui mais il y a un peu mieux, [LineageOS for microG](https://lineage.microg.org/). C'est un fork de LineageOS prévu pour fonctionner avec les gapps [microG](https://microg.org/), en clair, on peut toujours jouir des services de Google Play via des libs utilisant les API de Google, vous verrez dans le menu des applications, il y a une icone Paramètres pour configurer microG. De plus, cette version de LineageOS vient avec F-Droid pré-installé (et ce dernier, avec le F-Droid Privileged Extension, cela vous permet d'installer automatiquement les applications et activer l'auto-update). Pensez à [rajouter le dépôt de microG à F-Droid](https://microg.org/download.html), ca peut servir pour mettre à jour les composants de microG.

## Et le store ?

Plus de Google Play Store mais il y a, comme je vous le disais, le store F-Droid qui commence à disposer de pas mal d'applications, libres et/ou respecteuses de la vie privée (du moins sur la fiche c'est indiqué ce qui peut aller ou pas), on y trouve Firefox, Nextcloud, Caldav (gratuit alors qu'il est payant sur Gooogle Play), Telegram...Peut être que je ferais régulièrement un billet sur les applications intéressantes que je déniche sur F-Droid.

Après si vous voulez une application qui est sur le Google Play Store, par exemple, l'application de votre banque, et bien, il suffit d'installer depuis F-Droid, [Yalp](https://f-droid.org/en/packages/com.github.yeriomin.yalpstore/). Yalp est une application qui permet d'aller cherche l'APK de n'importe quelle application disponible sur le Google Play Store. En gros, vous aurez toujours la possibilité d'installer des applications du Play Store mais sans devoir passer directement par ce dernier. Pratique pour éviter d'être tracer. 

## Conclusion

Pour le moment, tout se passe bien. Je trouve même la tablette un peu plus rapide. Pour le moment, je ne vais pas faire la même chose sur le téléphone car je n'ai pas encore dévérouiller le secure boot et j'ai pas envie d'y toucher, mais qui sait plus tard peut être...




