---
title : "2017 l'année de Solus ?"
date : "2017-01-07T16:51:37+01:00"
draft : false
thumbnail : "img/solus.jpg"
toc : true # Optional
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---

On peut dire que ça chôme pas chez Solus. En effet, dès le 1er janvier, ils ont sortis une snapshot release [avec son lot de nouveautés](https://solus-project.com/2017/01/01/solus-releases-iso-snapshot-20170101-0/) dont le brisk menu pour Mate ou le software center totalement traduit en français cette fois.

Qui l'eut cru le projet date de 5 ans déjà:

<img src="http://lh3.googleusercontent.com/-94N3GZst9UM/WGzOjZuH1gI/AAAAAAAAiEg/EI1XAPvdBtEtaYxr71sccigIjt5nDEy0ACJoC/w530-h220-p-rw/Screenshot%2Bfrom%2B2017-01-04%2B10-28-27.png">

En plus, grâce à l'argent récolté sur [patreon](https://patreon.com/solus) ils ont pu [s'offrir du nouveau matos et un nouveau serveur dédié](https://solus-project.com/2017/01/05/early-2017-infrastructure-upgrades/) pour les dépôts. Un gain en performance:

<img src="http://lh3.googleusercontent.com/cR-7qXmoZf1nB_QDZlLDu7vmUD9lmmGTdW1qZ5riIdzVspUt0B9lKJwrJUB5TWtHwB9ALLEYeLA5gw=w1920-h1080-rw-no">

Pour début 2017, la team n'a que deux objectifs et pas des moindres ! Ils veulent mettre le paquet sur une solution graphique de permutation de Pilotes linux, en particulier pour le support NVIDIA Optimus. Après cela, le second objectif sera la version 11 de Budgie desktop avec un tas d'améliorations comme de meilleurs animations et une refonte de Raven, le panneau de notification.

Bref, un projet à surveiller de très près !
