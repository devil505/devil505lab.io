---
title : "Dégooglisons phase 6 (et dernière)"
date : "2018-02-19T08:48:00+01:00"
draft : false
thumbnail : "img/edito.jpg"
toc : true # Optional
tags : ["editos"]
categories : ["editos"]
---

Nous voici à la 6eme et dernière phase qui regroupe plusieurs passages.

On s'attaque au moteur de recherche. Stop Google.fr qui traque mes recherches, après avoir testé qwant, duckduckgo et framabee (instance searx), mon choix s'est porté sur [ixquick](https://www.ixquick.fr/), une instance statpage). Les résultats sont les mêmes que ceux de Google donc pas de surprise mais les requêtes sont anonymes (normalement) et ixquick ne garde pas votre IP stockée.

Maintenant, les réseaux sociaux, laisser tomber Facebook et Google+. Bon, je me suis laissé tenté par diaspora et son instance sur framasphere. Framasphere c'est sympa mais au final il n'y a pas grand monde ou alors que des libristes et/ou linuxiens. Bon, pas grave, être sur un réseau social n'est pas forcément primordial.

En bonus, pou se débarrasser de twitter, il y a mastondon. Je m'étais inscrit sur une instance de framapiaf, c'est plutôt sympa. J'y ai retrouvé pas mal de conaissances linuxiennes.

Pour les traductions, j'avais pour habitude d'utiliser le traducteur de Google. C'est kyrios de Solus qui m'a suggéré une alternative, [Deepl](https://www.deepl.com/translator), qui semble efficace et meilleur en plus !

Tout comme FredBezies, je n'ai pas trouvé de véritable alternative à Google Play Musique, trouver un stockage de 20 000 titres libre et gratuit avec accès aussi bien sur PC que sur smartphone/tablette c'est pas évident.

Y'a t'il alternative à Youtube ? Là aussi, c'est compliqué mais le projet PeerTube a décollé et on peut trouver un serveur de test [ici](https://peertube.cpy.re/). D'ici quelques temps, on pourra j'espère trouver des instances peetube un peu partout.

Conclusion, cela s'est pas trop mal passé. J'avoue encore utiliser ma boîte gmail mais la transition prendra du temps et cette boîte peut me servir de poubelle pour les mails peu intéressants plus tard. Je suis assez satisfait des services alternatifs et libres via framasoft ou encore cozy. Maintenant à votre tour :-)
