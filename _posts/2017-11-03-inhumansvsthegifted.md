---
title : "Inhumans VS The Gifted"
date : "2017-11-03T18:29:00+02:00"
draft : false
thumbnail : "img/seriestv.jpg"
toc : true # Optional
tags : ["séries TV"]
categories : ["séries TV"]
---

Avec la rentrée des séries, il y a un duel entre deux séries de l'univers Marvel. D'un côté, **Marvel's Inhumans** de Marvel Studios et de l'autre, **The Gifted** de la Fox. Alors laquelle faut il regarder ?

### Marvel's Inhumans

Si vous avez suivi Marvel Agents of Shield, vous devez connaître les Inhumans. Peuple qui avait autrefois colonisé la terre et qui ont chacun des pouvoirs après avoir été en contact avec le terrigène. Bon, et bien ce peuple se cache en fait dans une base furtive sur la Lune. C'est en fait une monarchie et, sans trop vous en dévoiler, il y aura une lutte du pouvoir.

<iframe width="560" height="315" src="https://www.youtube.com/embed/1sYF1SXcWqQ" frameborder="0" allowfullscreen></iframe>

La série se laisse regarder sans pour autant être accrocheuse. L'audience n'est pas au top et le risque de flop est très important. Les séries Marvel diffusées sur ABC ne sont pas réputées pour avoir une grande qualité (à l'inverse de celles sur Netflix) même si Agents of Shield peut surprendre parfois.

### The Gifted

Après avoir pris un risque osé avec Legion, la Fox retente le coup avec une nouvelle série de l'univers X-Men. The Gifted relate ni plus ni moins la persécution de mutants dans des États Unis très sectaires vis à vis des gens avec des pouvoirs. Il y a des références aux X-Men ainsi que la confrérie de Magnéto qui ont disparus tout les deux (c'est plus simple et ça coûte moins cher). 

<iframe width="560" height="315" src="https://www.youtube.com/embed/bvyJfspGN3E" frameborder="0" allowfullscreen></iframe>

La série me semble plus attractive, surtout pour le côté _mutant qui a la vie dure_ comme l'un des personnages qui se retrouve en prison avec un collier qui bloque ses pouvoirs.

### Alors on regarde ?

Bah si vous vous ennuyez, je dirais oui. Dans le cas contraire, je dirais que The Gifted a plus de chances de survie pour obtenir une saison 2.
