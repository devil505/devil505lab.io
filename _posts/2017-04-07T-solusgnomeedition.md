---
title : "Bientôt une édition GNOME de Solus ?"
date : "2017-04-07T17:07:55+02:00"
draft : false
thumbnail : "img/solus.jpg"
toc : true # Optional
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---

Et bien, c'est ce qu'on pourrait croire en jetant un coup d'œil au [dépôt git des images Iso](https://git.solus-project.com/images/gnome/) du projet Solus.

Donc en y regardant de plus près, on devrait avoir un GNOME de base avec le software center et l'application de recherche de pilotes de Solus.

Comme pour les autres éditions, par défaut ,ce sera le duo Firefox et Thunderbird comme navigateur et courrielleur. Le thème arc est de la partie, c'est celui utilisé par défaut chez Solus. La suite bureautique sera Libreoffice.

Pourquoi une telle version ? Après réflexion, peut-être que Ikey Doherty a pensé que certains utilisateurs risquent de regretter le côté Gtk de la prochaine version de Budgie qui utilisera Qt. Dans ce cas, une version avec Gnome peut se justifiée surtout si la mise en place se fait facilement sans fioritures.
