---
title : "nouvelles avril 2017"
date : "2017-04-06T16:19:33+02:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux"]
categories : ["linux"]
---

Vous avez sans doute dû le voir, la grosse news du moment c'est l'abandon pur et dur d'unity par Canonical.

Et oui, Mark Shuttleworth a enfin compris avec même une certaine humilité que développer un environnement de bureau détesté par une partie de la communauté n'était pas une bonne idée. Par conséquent, la saveur principale d'ubuntu sera GNOME (bien évidemment dans sa version 3 avec gnome-shell) pour la prochaine LTS. Cela me paraît légitime, au départ c'était GNOME l'environnement principal, à l'époque c'était même GNOME dans sa version 2, les râleurs diront qu'il fallait prendre Mate. Pourtant Okki explique bien pourquoi ce n'est pas un mauvais choix

mais depuis l'arrivée d'unity, gnome a évolué dans sa version 3 et c'est un peu ce qui fait avancer tout le monde (gtk, fedora... Etc). Okki le précise bien [sur son blog](https://www.gnomelibre.fr/2017/04/pourquoi-gnome/). Cela fera l' affaire de tout le monde à mon avis, finie la guerre des patchs Ubuntu non-validé par l'équipe de GNOME.

Le projet Mir sera sans doute aussi abandonné au profit de Wayland. Du coup, adieu l'Ubuntu phone et la convergence, là aussi Google avec Android règne avec suprématie et la firme Samsung tente déjà de proposer la convergence avec ses derniers Galaxy S.

Le bon côté de tout ça ? Comme [l'a dit Adrien de Linuxtricks](https://www.linuxtricks.fr/news/10-logiciels-libres/330-coup-de-tonnerre-ubuntu-arrete-le-developpement-de-unity/), cela va permettre de se concentrer sur un seul projet. Et encore, c'est pas dit qu'ubuntu contribue efficacement au projet GNOME. Je pense qu'uUuntu fera essentiellement un travail de personnalisation des ISOs et se synchronisera sur Gnome pour les sorties, bref on pourrait à terme avoir Fedora debiannisée. La grosse différence se fera ailleurs. En effet, Canonical veut mettre le paquet sur les technologies du Cloud pour entreprises et l'IoT (Internet of things) grosso modo, les objets connectés. Faut pas se leurrer, la politique de Canonical se recentre sur des solutions commerciales, faut bien gagner de l'argent à un moment donné.

Pour les utilisateurs de tout les jours, Ubuntu risque de devenir une distribution qui se démarquera de moins en moins des autres. Sa popularité avait déjà pris un sérieux coup avec Linux Mint en plus. Assistons nous à la fin d'un mythe ? on le découvriras dans les prochains mois.

Et pendant ce temps, [l'équipe elementary s'est retrouvée à Denver](https://medium.com/elementaryos/the-denver-appcenter-sprint-85f12a667e03). Pour résumer, ils ont travaillés sur l'avenir de l'AppCenter qui je pense est bien parti pour devenir un Store de smartphone sur PC, de plus, le dépôt du projet migre [sur github](https://github.com/elementary/appcenter) (adieu launchpad, la plate-forme de Canonical) là où sont déjà un tas de projets comme Solus ou Khali Linux. L'abandon d'unity permettra t'il à elementary OS (avec sa volonté de révolutionner l'expérience utilisateur) à croître davantage ? On verra bien.

Je vous laisse avec les opinions du fondateur d'elementary OS et de celui de Solus sur Goougle + sur la news du moment ;-)

<img src="../../img/ubuntu_unity_danfore.jpeg">

<img src="../../img/ubuntu-unity-ikey-doherty.jpeg">
