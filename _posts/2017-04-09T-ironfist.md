---
title : "Iron Fist"
date : "2017-04-09T11:38:06+02:00"
draft : false
thumbnail : "img/seriestv.jpg"
toc : true # Optional
tags : ["séries TV"]
categories : ["séries TV"]
---

Voilà la dernière série que j'ai terminée. Iron Fist est la quatrième série Marvel de Netflix, après Daredevil, Jessica Jones et Luke Cage. Mon avis est que ce n'est pas la meilleure des quatre mais ce n'est pas mauvais pour autant. Si vous avez aimé les autres il est conseillé de la visionner car cet été Netflix sortira la première saison de The Defenders qui réunit les quatre héros, on peut s'attendre un _Avengers_ sous forme de série TV.

Le pitch, Danny Rand porté disparu dans le crash d'un avion avec ses parents dans l’Himalaya réapparaît 15 ans plus tard. Son père gérait Rand Entreprises dont la direction a été repris par son associé Meachum décédé (ou pas) d'un cancer peu de temps après. Donc, Danny réapparaît pour reprendre sa société maintenant dirigée par Ward et Joy, les enfants de Meachum, qui furent ses compagnons de jeux durant son enfance. Cela va, au début, pas être simple mais le plus mystérieux est bien sûr ce qu'il lui ait arrivé durant ses 15 années. Il a suivit un entraînement dans un monastère où il a acquit le pouvoir de l'Iron Fist.

<iframe width="560" height="315" src="https://www.youtube.com/embed/f9OKL5no-S0" frameborder="0" allowfullscreen></iframe>

La saison est consacrée à la reconquête de Rand Enterprises par Danny mais aussi à son combat face à une puissante organisation criminelle _La main_ déjà aperçue dans Daredevil.

Finn Jones joue le rôle de Danny Rand très bien, il était ser Loras dans _Game of Thrones_. Mention pour la méchante Madame Gao vue dans Dardevil qui est vraiment badass malgré son âge. Un personnage non négligeable est celui de Colleen Wing, partenaire de Danny dans son combat.

<iframe width="560" height="315" src="https://www.youtube.com/embed/MTH0Hki6hRE" frameborder="0" allowfullscreen></iframe>

Les combats ne sont pas tous géniaux, je préférais ceux de Dardevil. Au final, la série se laisse quand bien regarder.

Rendez-vous cet été pour The Defenders.

<iframe width="560" height="315" src="https://www.youtube.com/embed/N6iLdoTUtpc" frameborder="0" allowfullscreen></iframe>
