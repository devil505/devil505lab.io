---
title : "Les Orphelins Baudelaire"
date : "2017-02-19T08:59:40+01:00"
draft : false
thumbnail : "img/seriestv.jpg"
toc : true # Optional
tags : ["séries TV"]
categories : ["séries TV"]
---


Netflix a récemment proposé la première saison des **Désastreuses Aventures des orphelins Baudelaire** que j'ai bien évidemment visionnée en entier. La série est tirée d'une œuvre littéraire de plusieurs tomes et il y avait même eu une adaptation au cinema avec Jim Carrey.

Le pitch: trois enfants, Violette, la plus grande et la plus ingénieuse, Klaus, qui passe son temps à lire (et à tout retenir des ses lectures) et, Prunille (Sunny en VOST), la benjamine qui est encore un bébé mais qui a malgré tout une très bonne dentition, perdent leur parents dans l'incendie de la maison familiale. Le destin des 3 enfants Baudelaire changent radicalement pour devenir très sombre car ils vont être pris en charge par un lointain parent, le Compte Olaf, qui est absolument ignoble avec eux et qui ne pense qu'à s'emparer de l'héritage des Baudelaire.

L'univers où se déroule l'action est un peu surréaliste, cela fait penser un peu à Tim Burton. Les personnages sont vraiment très bien joués, certain sont de véritables caricatures comme par exemple le banquier qui est d'une bétise absolue. La distribution d'acteurs est franchement pas mal.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tup-5yOcJuM" frameborder="0" allowfullscreen></iframe>

Le plus de la série c'est bien sûr Neil Patrick Harris qui interprète ici l'un de ses plus grand rôle à mon avis. Il change de registre par rapport à How I met Your Mother. Il est parfait dans le rôle du Comte Olaf qui est un personnage en fait assez complexe car ce dernier est amené à se déguiser et donc jouer des personnes différentes. C'est même lui qui chante le générique du début *Look Away*, *regardez ailleurs* mais que l'on peut traduire dans ce cas là par *Éteignez votre télé*, car oui la série à un ton vraiment humoristique malgré la gravité de certaines situations.

L'auteur des livres, Lemony Snicket, est également présent dans la série et joue un rôle de narrateur. Il est brillement interprété avec humour et gravité là encore. On peut même le nommer Mr Spoiler car dès le début il insiste pour signaler au téléspectateur que l'histoire finira mal !

<iframe width="560" height="315" src="https://www.youtube.com/embed/6hlNVt-STn4" frameborder="0" allowfullscreen></iframe>

Cette série est vraiment originale, bravo à Netflix. Il y a même dans l'avant dernier épisode, un petit twist scénaristique qui ne laisse pas indifférent. Franchement si vous cherchez une série pleine de rebondissements avec un peu d'humour, foncez !
