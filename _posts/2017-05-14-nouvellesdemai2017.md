---
title : "Nouvelles de Mai 2017"
date : "2017-05-14T09:06:14+02:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux"]
categories : ["linux"]
---

Finalement j'avais bien deviné, Solus à bien [sorti une version GNOME](https://solus-project.com/2017/04/18/solus-releases-iso-snapshot-20170418-0/).

Le Software Center de Solus est entrain d'être amélioré avec l'affichage de plusieurs captures d'écrans et du changelog sur les fiches de présentation des applications.

![](https://lh3.googleusercontent.com/-VJuvgrWAKro/WRev0BQuQGI/AAAAAAAAKXk/7GG50Ma7dBAjInNazAe5dyEZC5Gyr6HUQCJoC/w530-h298-p-rw/Screenshot%2Bfrom%2B2017-05-14%2B02-12-00.png)

![](https://lh3.googleusercontent.com/-XQe4I_2K67c/WRfPzWf0dbI/AAAAAAAAKYQ/uXC3p45WWTAsMSj7phGFVulEYzV8nW5xgCJoC/w530-h298-p-rw/Screenshot%2Bfrom%2B2017-05-14%2B04-31-17.png)

J'ai aussi crée [une communauté francophone dédiée à Solus sur Google+](https://plus.google.com/b/109348840800096254191/communities/101371177200990436142), n'hésitez pas à venir !

Je me suis récemment rendu compte que [Solus touche plus de 1400$ par mois](https://www.patreon.com/solus), plus qu'elementary actuellement (900$), bien joué !

Tiens parlons d'elementary, aux dernières nouvelles, [l'ouverture de l'AppCenter aux développeurs tiers suit son cours avec l'apparition de nouvelles applications](https://medium.com/elementaryos/appcenter-spotlight-more-beta-testers-d9743aec2619).

Sachez également que les utilisateurs réclament de nouvelles applications comme une application pour gérer les contacts, un client pour Ring et enfin des outils de développement Web. Est ce que la mayonnaise va prendre ? On verra bien...
