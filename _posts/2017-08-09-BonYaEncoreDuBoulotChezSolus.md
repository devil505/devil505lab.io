---
title : "Bon, y'a encore du boulot à faire chez Solus"
date : "2017-08-09T13:42:00+02:00"
draft : false
thumbnail : "img/solus.jpg"
toc : true # Optional
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---


Ce titre s'explique avec la mise en ligne d'une Todo liste par Ikey et disponible [ici](https://dev.solus-project.com/T4235). Voici les points que je retiens.

## Vraiment s'y mettre à ferryd

En gros, ferryd est un logiciel qui permettra de livrer de manière sûre les paquets sur le dépôt officiel utilisé par les utilisateurs. Ikey semble bien déterminé à rendre ce projet opérationnel.

## Flatpak...oh et puis on vas prendre Snap aussi

Il y a quelques mois, Solus annonçait avoir opté pour Flatpak et rejeté Snap, et bien non, il y aura finalement les deux. Ce qui permettra plus de choix. Moi j'ai déjà utilisé les deux, le nombre de paquets est encore limité et une interface graphique serait un plus. Espérons que le Solus software center puisse gérer les deux formats.

## Exit eopkg, bienvenue sol

Oui, il est temps de laisser le Package Manager issue d'evolve OS ou dérivé de pisi pour sol, le véritable package manager de Solus.

## Plus de mainteneurs

Solus manque de mainteneurs, les développeurs ne sont pas si nombreux que ça. Ça explique pourquoi les mises à jour sont si espacées. J'aimerais en profiter pour signaler qu'il y a un contributeur francophone (belge) très actif (et aussi très sympa car j'ai déjà discuté avec lui sur IRC), il s'agit de [kyrios](https://dev.solus-project.com/p/kyrios123/), vous pouvez aussi le retrouver sur le forum officiel de Solus. Je ne serai pas surpris qu'il devienne mainteneur.

## Une meilleure gestion des GPU hybrides

C'est un truc qui m'intéresse de plus en plus car je compte m'offrir un Xiaomi Mi Air 13 qui a un GPU hybride Intel et NVIDIA. Kyrios m'avait dit que la gestion d'Optimus sous Solus n'était pas totalement au point alors que le blog vantait les mérites de linux-driver-management. Là aussi, j'espère que cela va s'améliorer.

## Conclusion

Ikey parle aussi d'un tas de choses à améliorer et cela va de bon sens. J'espère sincèrement que les items de cette Todo list seront réalisées car le problème chez Solus c'est qu'ils ont beaucoup (trop ?) d'idées mais la main d'oeuvre n'est pas toujours à la hauteur de leurs ambitions. Enfin, c'est surtout mon ressenti. Si vous voulez contribuez, n'hésitez pas 😉
