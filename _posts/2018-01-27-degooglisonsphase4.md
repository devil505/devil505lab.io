---
title : "Dégooglisons phase 4"
date : "2018-01-27T10:21:00+01:00"
draft : false
thumbnail : "img/edito.jpg"
toc : true # Optional
tags : ["editos"]
categories : ["editos"]
---

Cette fois, on va s'attarder sur la prise de note. Avec Google, j'utilisais Keep, super pratique pour prendre des notes, mettez de côté un lien...etc

J'ai trouvé une alternative avec [framanotes](https://mes.framanotes.org/). Service gratuit pour prendre des notes et le markdown est même supporté, je l'utilise donc pour mes brouillons de billets pour ce blog. Framanotes utilise une instance [turtl](https://turtlapp.com/), il est donc possible d'utiliser l'application android sur smartphone et tablette. Pour linux il y a toujours le moyen de passer par le navigateur mais j'ai quand même réussi à packager l'application linux pour Solus (je mettrai le paquet bientôt sur le dépôt).

Keep me permettait également de planifier des tâches. Pour les tâches, je passe par Nexcloud que j'utilise via framagenda comme je l'expliquais dans un précedent billet. Sur smartphone/tablette avec le store F-Droid, il faut installer l'application [OpenTasks](https://f-droid.org/en/packages/org.dmfs.tasks/). DAVDroid se charge de la synchro comme on avait vu précédemment. Avec OpenTasks, on peut créer des tâches avec un tas d'infos, date limite...etc. Coté PC, sous SOous Budgie, j'ai installé Gnome-TODO qui m'affiche les tâches de mon compte Nexcloud/Framagenda. La syncho se fait alors dans les deus sens.

Conclusion, dégooglisation plutôt réussie :-)
