---
title : "Soucis de transparence chez Solus"
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---

L'équipe de Solus vient de publier [un billet](https://getsol.us/2018/10/27/in-full-sail/) pour parler de l'état actuel de la distribution. On vas traduire quelques morceaux et les commenter.

Josh et Bryan ont résumés l'historique de Solus. Voici quelques parties traduites (plus ou moins bien avec deepl):


> Joshua : Avec la sortie de ferryd, nous avons été en mesure d'effectuer de manière plus agressive des mises à niveau de cheminées, ce que nous avons fait pour les prochains mois, tels que GNOME 3.26.1, LLVM 5, et plus en octobre 2017. Nous avons également terminé l'année avec notre Welcome to the Grid Gamefest, qui a mis à l'épreuve notre tout nouveau support LSI et Steam à base de snap. C'était aussi le dernier liveream auquel Ikey a participé. C'est à cette époque que, du moins à mon avis personnel, il est devenu plus évident qu'il y avait divers changements dans son comportement et sa personnalité qui m'ont donné l'impression qu'il allait apporter divers changements à sa vie personnelle.
> 
>Bryan : Je ne peux pas vraiment dire que j'ai remarqué quelque chose à l'époque, mais encore une fois, je ne connais Ikey que depuis un peu plus de 2 ans. Même là, je pense que le voir passer à de nouveaux projets comme Linux Driver Management, en janvier, et la réécriture du Software Center, en février 2018, nous aurions pu oublier ces préoccupations. D'un point de vue personnel, Ikey et moi avons passé la majeure partie de février et de mars en compétition dans notre quête pour perdre du poids. Nous mangions tous les deux beaucoup plus sainement et nous faisions beaucoup d'exercice. Ce que je peux dire, c'est que je ne suis pas surpris qu'à la fin mars, Ikey ait décidé de quitter Late Night Linux. Il était clair pour moi qu'avoir à passer un temps non négligeable toutes les deux semaines pour monter un spectacle était plus que ce qu'il avait négocié. De plus, à l'époque, Linux connaissait une atmosphère inhabituellement chargée politiquement, avec la toxicité de la politique mondiale qui commençait à se manifester dans nos cercles généralement moins chauds. La situation s'est tellement aggravée que nous avons été forcés d'interdire strictement à certaines politiques d'être discutées hors sujet et dans nos propres canaux de développement. Pour Ikey, il s'agissait d'une période pendant laquelle on le faisait parader d'un balado à l'autre en raison de ses opinions bien arrêtées et, franchement, de sa capacité d'attirer une foule. C'est difficile pour moi de ne pas voir ça comme des gens qui exploitent le statut d'Ikey pour leur propre profit. Toute l'équipe centrale était tout à fait d'accord avec sa décision de se retirer de tout cela.
> 
> Joshua : En dehors de Late Night Linux, Ikey avait décidé de ne plus participer à diverses plateformes de médias sociaux, en particulier Reddit et Twitter, et de doubler son cheminement vers l'amélioration personnelle. Il a commencé à parler davantage en public de divers aspects de sa vie personnelle, comme le fait de travailler en vue d'obtenir son permis de conduire et de mener une vie plus saine, ce qui a mené l'équipe centrale à discuter en privé avec lui de notre capacité d'assumer plus de responsabilités et de rôles mieux définis. Nous avons compris qu'il portait un lourd fardeau sur ses épaules en tant que seul développeur à temps plein.
> 
> Joshua : Le 3 juillet 2018, Ikey nous a informés que certains événements personnels de sa vie allaient le ramener en Angleterre. Il a commencé les préparatifs et le 11 juillet, Bryan, Ikey et moi-même nous sommes assis pour discuter de l'élimination de divers problèmes liés au facteur bus. Au départ, cela nous assurait un accès complet aux serveurs OVH, à savoir le constructeur, ring0 (notre serveur de paquets) et le serveur web (auquel j'avais déjà un accès SSH). L'intention était de faire un suivi le lendemain sur le domaine (géré par Dediserve), Google Apps for Business (pour email et documents), Fastly, et plus encore. Malheureusement, en raison de divers horaires conflictuels, comme ma famille en visite des États-Unis, Bryan étant occupé au travail et Ikey se préparant à déménager, ces discussions n'ont jamais été reprises.
> 
> Bryan : Le 13 juillet 2018, j'ai eu ma toute première synchronisation du vendredi. Un jour ou deux avant, Ikey et moi avions convenu qu'il s'agirait d'une période d'essai pour d'autres personnes assumant cette fonction particulière. Tout s'est bien passé, ce qui prouve que nous avions accès à tout ce dont nous avions besoin pour apporter des changements importants à Solus chaque semaine, sans avoir à faire appel à Ikey pour effectuer le changement final. A ce stade, j'avais également terminé ma deuxième semaine de mise à jour du noyau. Avec Josh et moi tous les deux occupés par la vie, la semaine suivante passa assez vite. Le 19 juillet, Ikey est officiellement parti pour l'Angleterre. Il a posté quelques photos et messages sur G+ à propos de son voyage et le reste de l'équipe a décidé de donner à Ikey tout le temps nécessaire pour s'installer dans son nouvel environnement.
> 
> Joshua : D'après mon expérience personnelle, en tant que personne qui a quitté les États-Unis pour la Finlande en janvier 2014, je comprends parfaitement à quel point les premières semaines à un mois peuvent être stressantes et chaotiques pour s'installer en Finlande. Nous voulions rester conscients du fait que cette période pourrait être très stressante pour Ikey. Le 30 juillet, Ikey a pris contact via IRC sur notre chaîne publique hors-sujet ainsi que sur Google+ à propos de son horrible Internet mobile.
> 
> Bryan : Cela nous amène au mois d'août de cette année. Josh et moi avons décidé qu'un Hackfest était attendu depuis longtemps et que Summertime Solus était né. Au cours de ce parcours, nous avons franchi un nombre important de jalons et, personnellement, j'ai pu enfin sortir plusieurs logiciels que j'avais passé plus d'un an à développer pour Solus. Il s'agit notamment de Cuppa (suivi des versions en amont) et d'eopkg-deps (commandes de reconstruction), qui ont toutes deux fait l'objet de mises à jour importantes ce jour-là. Josh a également travaillé sur plusieurs améliorations considérables de la qualité de vie de Budgie, ce qui en a fait le tout premier Hackfest où nous avons tous les deux contribué de façon significative au code Solus. Il était difficile d'ignorer l'absence d'Ikey ce jour-là, avec beaucoup de gens qui nous demandaient si nous savions comment les choses allaient avec lui et quand il aurait enfin un véritable accès Internet. Nous savions qu'il ne se sentait pas bien depuis son arrivée en Angleterre, probablement à cause d'une sorte de grippe qu'il avait contractée dans la faune locale. Naturellement, nous avons décidé de lui donner le temps de se reposer et nous n'avons pas insisté sur la question de l'amélioration d'Internet. Rétrospectivement, c'est la même semaine qu'Ikey a effacé discrètement son Instagram et s'est inscrit à l'IRC alors qu'aucun d'entre nous n'était là.
> 
> Joshua : J'ai contacté Ikey le 16 août, seulement après quoi il a posté à Mastodon qu'il quittait effectivement la plate-forme. À ce stade, il n'avait pas encore affiché ses comptes de médias sociaux ou les avait entièrement supprimés.

> Tout cela est bien inquiétant au sujet d'Ikey. On apparend ensuite un peu plus sur le soucis des serveurs et le passage au domaine getsol.us.
> 
> Joshua : Avance rapide jusqu'au 27 août et Solus a connu l'une de ses premières pannes majeures de divers services, tels que le site Web, Phabricator, et les forums. Malheureusement, l'accès au panneau de contrôle mondial d'OVH n'avait pas été fourni et je n'ai pas été contacté par le service technique pour ce compte. En conséquence, OVH est restée plutôt réservée quant à la fourniture de détails sur les questions relatives au serveur. Après de multiples appels, ils ont finalement déclaré que c'était le résultat de la maintenance du réseau.
> 
> Après environ 5 heures de temps d'arrêt, Ikey m'a contacté et m'a informé qu'il s'agissait d'un problème OVH dû à des gelées dures. Il n'y a eu aucun autre contact de sa part avant la deuxième panne.
> 
> La deuxième panne s'est produite le 5 septembre. Encore une fois, sans accès adéquat, je n'ai pas été en mesure d'en arriver immédiatement à une décision sur la question. Nous sommes immédiatement passés à l'action, j'ai installé un miroir temporaire sur un domaine que j'avais précédemment acheté pour téléchargement direct, getsol.us, et j'ai informé notre communauté de suivre les médias sociaux et ce site Web pour plus d'informations. Vers 2 heures du matin le 6 septembre, j'ai pu entrer en contact avec une personne d'OVH et on m'a informé que les paiements par serveur n'avaient pas été effectués. Après avoir expliqué notre situation et le manque de contact avec Ikey, ils ont permis de remettre le serveur en ligne pour 5 jours supplémentaires. C'est à ce moment-là que Bryan et moi avons commencé à mettre en œuvre divers plans de migration et d'urgence.
> 
> Bryan : J'ai demandé à mes collègues du Rochester Institute of Technology de m'aider à nous remettre sur pied et à fonctionner. Nous avions déjà eu des discussions sur l'hébergement de matériel chez RIT for Solus et cela m'a facilité la tâche de demander du matériel à la retraite pour redémarrer l'infrastructure Solus. En fait, j'avais déjà commencé à mettre en place un second serveur de compilation pour Peter à utiliser à la lumière de son (terrible) ordinateur portable et des mises à jour de la pile (comme KF5). Mes collègues de travail m'ont fourni un autre serveur, celui-là avec beaucoup plus de mémoire, même s'il est un peu plus vieux. Je l'ai mis en place le plus rapidement possible pour servir d'environnement d'hébergement virtuel pour les services Solus, à savoir : le Build Server, le Package Server, et le Web Server. Une fois ce système opérationnel, j'ai concentré mon attention sur l'extraction des données des serveurs OVH et le retour à l'environnement Build.
> 
> Pendant que Bryan travaillait dur sur l'amorçage des différents environnements, comme l'environnement de compilation, j'ai commencé à mettre en place des services tels que les forums et le site Web, la configuration de certains aspects de nginx, ainsi que la suppression des dépôts Phabricator Git. Nous avons également consacré beaucoup de temps à informer notre communauté en rédigeant le billet du blogue Supercharging Solus Infrastructure et en restant en contact constant avec notre communauté grâce à nos comptes de médias sociaux.

Par la suite, on découvre malheureusement qu'Ikey aurait des soucis de santé.

> Bryan : Au milieu de tout cela, Ikey nous a contacté le 7 septembre pour nous informer qu'il ne se sentait toujours pas bien, mais qu'il avait payé le serveur pendant encore 30 jours pour que nous n'ayons pas à nous précipiter pour terminer la migration :
> 
>     "et je suis très très malade. Tout sera payé pour les 30 prochains jours et me donne le temps de sortir et de vous transférer. Je vous parlerai demain après-midi/soir".
> 
> À ce jour, c'est la dernière communication que nous ou toute personne en contact avec nous avons reçue d'Ikey.

Malgré cela, la team maintient le cap pour assurer le support envers les utilisateurs de Solus.

> Joshua : Le 9 septembre, notre outil de suivi du développement était en ligne. Avec l'attention de Bryan concentrée sur l'appel du constructeur, je me suis concentré sur l'implémentation des changements à eopkg / pisi pour déplacer gracieusement nos utilisateurs vers le dépôt de paquets RIT Solus (hébergé sur mirrors.rit.edu). Le 10 septembre, le constructeur était officiellement en place, recevant les constructions et les transmettant à notre nouveau dépôt. J'ai finalisé la mise à jour d'eopkg et l'ai ensuite publiée le 11 septembre dans l'ancien dépôt stable et annoncée via le blog "Package Repo Migration Now Available".
> 
> Joshua : C'est également à cette époque que, à mon insu, j'ai été contacté par diverses personnes pour faire des commentaires sur un podcast à venir. Faute d'informations concrètes sur Ikey, de compréhension de sa situation personnelle et soucieux d'assurer une expérience de mise à niveau transparente pour nos utilisateurs, nous n'avons tout simplement pas eu l'occasion de faire des commentaires avant que le podcast n'ait eu lieu. En raison de notre absence (involontaire) de réponse avant le podcast, plusieurs personnes ont mal interprété la situation, tant pour le projet que pour Ikey, ce qui m'a amené à donner une réponse plus officielle sur Reddit concernant la situation à ce moment-là, dans le respect de la vie privée d'Ikey. Avec le recul, il est facile de comprendre pourquoi les gens ont été frustrés par notre absence de réponse définitive. Cependant, nous croyions (et croyons toujours) que la meilleure ligne de conduite était de ne pas réagir avant d'avoir reçu plus d'information sur la situation ou jusqu'au moment où nous ne pouvions plus justifier d'attendre.
> 
> Joshua : La migration de l'infrastructure s'est officiellement achevée avec la sortie de Solus 3 ISO Refresh (également appelé Solus 3.9999). Après ce point, nous nous sommes concentrés sur l'amélioration de l'engagement de la communauté, des services de première partie et de l'offre aux utilisateurs de nouvelles fonctionnalités et de mises à niveau des piles (telles que le nouvel ISO pour les tests Plasma à partir de notre revue Shiny Delights). Pour mettre tout cela en contexte, en seulement 6 jours, nous avions entièrement redémarré l'infrastructure Solus, y compris une nouvelle version Solus.

On a lu pas mal de choses sur certains réseaux sociaux voire même le forum officiel. L'équipe a donc décidé de rompre le silence et donner de la transparence à ses utilisateurs.

> Bryan : Maintenant que vous avez une meilleure idée de la série d'événements qui ont précédé aujourd'hui, j'aimerais profiter de l'occasion pour partager avec vous notre raisonnement derrière les décisions que nous avons prises au cours des dernières semaines. Je le fais à la fois dans l'intérêt de la transparence et parce que je crois personnellement que vous avez le droit de l'entendre directement de notre part et non par les paroles d'autres groupes ou individus.
> 
> Il y a deux raisons principales pour lesquelles nous avons gardé un "Radio Silence" efficace en ce qui concerne Ikey et le statut de certains aspects du projet. Premièrement, il est important de reconnaître que même si les membres de l'équipe centrale ne placent pas Ikey sur un piédestal, nous avons beaucoup de respect pour lui en tant que personne et en tant qu'excellent esprit technique. C'est ce respect que nous avons exercé ces nombreuses semaines en lui donnant le temps de prendre soin de lui physiquement et mentalement. Cela signifie que nous avons choisi d'attendre patiemment qu'il fournisse ses propres réponses, plutôt que de recourir à la spéculation ou de l'encourager. Beaucoup d'entre vous voient probablement le silence qui en résulte comme un manque de jugement, un manque de confrontation, ou tout simplement comme de la " cagey ". Je voudrais répéter une fois de plus que notre silence est dû à notre respect mutuel pour Ikey en tant qu'être humain, pour sa vision pour Solus et pour les milliers d'heures qu'il nous a consacrées et pour le projet.
> 
> Bryan : Notre deuxième raison de garder le silence est beaucoup plus pragmatique. Après la dernière panne d'OVH, nous avons passé presque tous nos moments libres à remettre en service notre infrastructure et à faire tout ce qui était en notre pouvoir pour revenir à notre rythme antérieur en tant que projet. Cela signifiait se concentrer entièrement sur la migration, restaurer le service pour nos utilisateurs, et enfin pouvoir reprendre le développement au jour le jour. En raison de la migration, nous sommes maintenant à 100% en ligne, mais il nous a fallu presque tout le mois dernier pour rattraper le temps perdu. Presque ironiquement, je peux maintenant dire que nous sommes dans un meilleur état qu'avant les pannes d'OVH. Nous avons maintenant notre propre matériel pour l'infrastructure, nous ne sommes actuellement pas tenus de payer des frais d'hébergement et nous contrôlons Solus en totalité, de la proue à la poupe. Au cours des 30 derniers jours, nous avons clôturé plus de 300 Tâches sur le bug tracker et trié autant de Tâches dans leurs priorités et projets appropriés. Nous avons également réalisé avec succès 5 Friday Syncs, comprenant : la mise à niveau de la pile Xorg 1.20, l'introduction des derniers pilotes graphiques de la série Nvidia 410 pour les cartes RTX de Turing, et un Plasma Desktop Environment mis à jour maintenant disponible en version Public Beta ISO.

Le fait de ne plus être en contact avec Ikey pose certains problèmes car il était gestionnaire unique de certains services.

> Joshua : Pendant ce temps, j'ai été en contact avec une multitude de prestataires de services qui ont été traités soit via les comptes personnels d'Ikey, soit via le compte administratif Solus auquel l'équipe centrale n'a pas accès. Je veux fournir une liste claire des services qui étaient et/ou ne sont toujours pas accessibles à l'équipe centrale :
> 
    Dediserve est ce qui était utilisé pour le nom de domaine Solus et le DNS précédents. Sans cet accès, nous ne sommes pas en mesure de mettre à jour les différents paramètres DNS pour pointer vers nos nouveaux serveurs et, par conséquent, nous avons dû migrer vers getsol.us, probablement de façon permanente.
    Rapidement, c'est ce qui était utilisé avant notre migration pour notre référentiel CDN, permettant aux utilisateurs du monde entier d'accéder aux paquets sur un serveur géographiquement plus proche que le référentiel de paquets OVH.
    Google Apps for Business est ce qui a été utilisé pour notre e-mail ainsi que la collaboration de documents.
    OVH est ce qui a été utilisé pour héberger nos serveurs de compilation, de repo et web précédents. Comme expliqué précédemment, nous n'avions pas un accès complet (via le panneau de contrôle) pour effectuer les modifications nécessaires ou payer les services d'OVH. Heureusement, nous avons eu un accès partiel sous la forme de SSH.
    Patreon est ce qui a été utilisé pour les contributions financières mensuelles au projet Solus. Ce financement a servi à payer Ikey pour qu'il travaille à temps plein sur le projet et à fournir des fonds supplémentaires pour l'achat de matériel informatique, couvrant les coûts de service, et plus encore.
    Paypal est ce qui a été utilisé pour retirer de l'argent de Patreon. C'était sur le compte personnel d'Ikey.
    SendGrid est ce qui a été utilisé pour la distribution du courrier pour Phabricator et les forums.
> 
> J'ai pris contact avec toutes les parties susmentionnées, ce qui a abouti aux résultats suivants :
> 
    Dans le cas de Dediserve, ils ont refusé de donner accès au compte.
    Dans le cas de Fastly, leur soutien a été absolument incroyable et ils m'ont immédiatement fourni l'accès nécessaire au CDN Fastly. Nous avons la ferme intention de déplacer nos utilisateurs vers un référentiel de paquets sauvegardés par CDN et je tiens à les remercier une fois de plus pour leur réponse rapide et leur compréhension.
    Dans le cas de Google et Google Apps for Business, ils n'ont pas répondu à mes demandes parce que je n'avais pas le contrôle du compte administratif.
    Dans le cas d'OVH, ils n'avaient pas voulu nous fournir l'accès adéquat, mais ils étaient prêts à l'époque à laisser les serveurs en marche pendant 5 jours supplémentaires pendant notre migration. A ce stade, ils ne sont plus nécessaires.
    Dans le cas de PayPal, il s'agit du compte personnel d'Ikey et il ne m'est donc pas possible d'y accéder.
    Dans le cas de SendGrid, ils n'ont pas répondu à mes demandes. J'ai cependant créé un nouveau compte SendGrid pour la distribution du courrier, donc ce n'est pas un problème.

Parmis ces services il y a Patreon dont Ikey était le seul à gérer le compte. L'équipe recommande de ne plus donner de dons et demander directement à Patreon un remboursement.

> Joshua : Ce qui m'amène à Patreon, sans doute le sujet le plus difficile à aborder pour moi. Après plusieurs tickets de support, ils ont refusé à plusieurs reprises de donner accès au compte Solus. En ce moment, nous n'avons aucun moyen d'accéder aux fonds que bon nombre d'entre vous ont gracieusement donnés, et nous ne savons pas non plus quels fonds sont disponibles via Patreon. Je comprends tout à fait que si vous êtes frustré d'avoir contribué financièrement au Patreon, sans que nous puissions utiliser ces fonds pour acheter du matériel et payer les services, c'est frustrant pour nous aussi. Malheureusement, avec le refus de Patreon de nous aider, notre seule option est de vous demander de cesser immédiatement vos dons. Mon conseil personnel serait de contacter également Patreon pour les remboursements et de lui faire part de votre frustration pour son refus de nous aider. Nous ne pouvons qu'espérer qu'ils décident de nous donner accès aux fonds ou au compte.
> 
> Je m'excuse profondément que nous n'ayons pas pris les mesures nécessaires plus tôt pour que ce compte soit accessible à toute l'équipe. À l'avenir, nous n'accepterons aucun don en argent tant que nous n'aurons pas mis en place des mesures pour nous assurer que toute l'équipe pourra y avoir accès en tout temps.

Sans nouvelles d'Ikey, Joshua et Bryan rendent hommage au créateur de Solus.

> Bryan : La vision d'Ikey pour Solus est une vision que nous partageons tous au sein de l'équipe centrale. Solus est une obsession égoïste et pragmatique de construire une distribution linux techniquement excellente. C'est cette vision qui nous a tous attirés vers le projet en premier lieu. Chacun d'entre nous apporte son expérience et son expertise uniques, et le mentorat phénoménal d'Ikey au fil des ans nous donne une base incroyable sur laquelle nous pouvons bâtir.
> 
> Certes, ce qui m'a d'abord amené à Solus, c'est en fait l'environnement Budgie Desktop. A l'époque, Solus était encore EvolveOS et, j'ai honte de l'admettre, j'étais un fervent utilisateur de XFCE sur Ubuntu Studio. Le développement de XFCE s'étant arrêté, j'étais à la recherche d'un nouvel environnement de bureau et Budgie s'est montré remarquablement prometteur. En creusant plus loin, j'ai été surpris de constater que " ce type Ikey " y travaillait depuis des années et qu'il gérait également sa propre distribution Linux de base. Je devais en savoir plus. L'année suivante, j'ai gardé un œil sur le développement d'EvolveOS et de Budgie, lisant avec avidité les articles du blog pour tout nouveau détail, même si EvolveOS est devenu Solus. C'est au printemps 2016 que j'ai fini par tomber en panne et dépoussiérer mon client IRC et que j'ai commencé à traîner sur les chaînes Solus sur Freenode. Pendant la Solus 1.2 Hack-fest en juin, je me suis finalement sentie à l'aise pour engager la conversation avec les utilisateurs de Solus et l'équipe centrale. Ironiquement, l'un de mes premiers souvenirs de ces conversations a été un argument extrêmement nerd entre Josh et moi sur la façon dont fonctionnent les Asynchronous Compute Shaders dans les GPU Radeon. J'ai gagné.
> 
> Au travail, je passais mes heures de bénévolat à aider les chercheurs en construisant et en entretenant certains des logiciels qu'ils utilisaient. Donc le saut entre ça et mon apprentissage du packaging (surtout en voyant comme c'était facile avec ypkg), n'était pas si loin. Alors, j'ai sorti une ancienne machine de l'étagère, excitée par la première installation de Solus et je me suis retrouvée face à des choses ridicules comme les pilotes d'imprimante et la distribution massive de publication TeXLive LaTeX. Après avoir contribué plusieurs paquets via le Bugzilla Bug Tracker de l'époque, j'ai décidé qu'il était temps de découper un coin de Solus sur lequel j'allais travailler : Jeux. Je sentais que Solus manquait totalement de jeux open-source, alors j'ai fait ma vendetta personnelle pour arranger ça et les Solus Game-Fests étaient nés ! Pendant ces premiers flux YouTube, je n'étais qu'un membre de la communauté, mais je passais de 5 à 17 heures par week-end à emballer de nouveaux jeux pour les dépôts Solus. A ce moment-là, j'étais déjà en communication assez constante avec l'équipe centrale et nous avions commencé à "traîner" sur notre serveur de mumble. Après plusieurs émissions en direct, Ikey et Josh se sont finalement effondrés le 26 octobre 2016 et nous ont invités, Peter et moi, à nous joindre à l'équipe centrale, faisant remarquer qu'ils auraient vraiment dû le faire plus tôt. Tout d'un coup, les choses avaient changé pour moi pour toujours. Aujourd'hui, je viens de célébrer mon deuxième anniversaire en tant que membre de l'équipe centrale. Avec le recul, je suis extrêmement fier de tout ce que nous avons accompli en si peu de temps et reconnaissant pour tout le soutien que j'ai reçu de l'équipe centrale et de la communauté Solus. Merci !
> 
> Joshua : J'ai commencé à travailler sur ce projet peu après la sortie d'EvolveOS Beta 1. J'ai d'abord été attiré par le projet pour plusieurs raisons, certaines étant plus superficielles que d'autres, je l'admets. Budgie 8 ressemble à Chrome OS, qui était un design qui m'a assez attiré à l'époque. Pour des raisons plus techniques comme l'accent mis sur une architecture unique (à l'époque sans support pour les packages emul32, donc sans Steam ni WINE) et le fait d'être construit à partir de zéro avec des périphériques informatiques domestiques à l'esprit. C'était aussi (et c'est toujours) le seul système d'exploitation avec lequel mon vieil ordinateur portable a pu être correctement suspendu, ce qui était remarquable en soi. Bien sûr, cela ne compensait pas les quelques paquets hilarants de l'époque. Il n'avait pas d'Atom, donc je n'avais pas vraiment un IDE confortable pour l'édition. Il n'avait pas de mono, ce qui signifiait pas de Keepass donc je ne pouvais même pas ouvrir ma base de données de mots de passe. Mais il avait Ikey, sa vision, et sa volonté de consacrer d'innombrables heures à m'enseigner ypkg et le tout nouveau format d'emballage package.yml, pour que je puisse lui envoyer des correctifs par e-mail afin qu'il puisse les inclure dans notre dépôt monolithique alors unique git. EvolveOS (et plus tard Solus) a été le premier vrai projet Linux où je me suis senti vraiment habilité à faire une différence et à aider les autres. J'étais là quand nous avons formalisé la première équipe pour EvolveOS (Ikey, Justin, et moi-même). Je me souviens des conversations avec Ikey où j'étais absolument catégorique sur le fait que nous avions une forme de notification et un centre de widgets pour la nouvelle version de Budgie. C'est moi qui ai annoncé la première sortie (et la version candidate) de Solus et j'ai pris la place de responsable de la communication. Je me souviens m'être assis là, en train d'examiner et d'atterrir les paquets de Bryan dans ses jeux-fest avant qu'il ne devienne un membre de l'équipe de base. Je me souviens d'avoir discuté avec Ikey de l'intégration de Bryan, Peter et Stefan dans l'équipe centrale. J'y suis allé, j'ai eu ces discussions et nous avons poussé de l'avant ensemble parce que notre équipe diversifiée a une vision commune et des objectifs communs.
> 
>  Joshua : La vision commune de ce qu'est fondamentalement Solus est demeurée inchangée au fil des ans et a servi d'étoile du Nord pour nous guider. Ce que Solus peut être a été notre catalyseur de changement à mesure que notre communauté, nos besoins et notre équipe ont évolué.
> 
> Solus est égoïste.
> 
    Nous nous concentrons uniquement sur les appareils informatiques domestiques.
    Nous nous concentrons sur la prise en charge des architectures matérielles dominantes pour les périphériques que nous utilisons.
    Nous nous concentrons sur la construction d'un système d'exploitation que nous voulons et que nous sommes fiers d'utiliser.
> 
> Solus est pragmatique.
> 
    Nous ne nous limitons pas à une seule philosophie.
    Nous conservons le contenu de notre référentiel pour nous assurer que les utilisateurs installent des logiciels qui sont bien intégrés dans le système d'exploitation, ainsi que pour assurer la maintenabilité et la durabilité.
    Nous fournissons des logiciels à code source fermé parce que cela offre une expérience optimale à nos utilisateurs, même si nous préférons l'open-source dans la mesure du possible.
> 
> Solus est une obsession.
> 
    Nous sommes obsédés par l'idée de fournir des expériences et des défauts sains d'esprit prêts à l'emploi, dont certains peuvent aller à l'encontre de leurs homologues en amont (comme GNOME).
    Nous sommes obsédés par l'idée de fournir une plate-forme réactive, performante et accessible à tous les utilisateurs, quel que soit leur niveau d'expérience ou de compétence.
    Nous sommes obsédés par la sécurité, la convivialité et l'environnement de travail de nos utilisateurs.
> 
> Solus est techniquement excellent.
> 
    Nous n'hésitons pas à repenser des aspects entiers de notre architecture pour nous offrir, à nous et à nos utilisateurs, une meilleure expérience, qu'il s'agisse de gestion des pilotes, de support Steam ou de livraison de logiciels.
    Nous développons les outils dont Solus a besoin parce que rien d'autre n'est assez bon pour répondre à ces exigences.
> 
> Solus est un système d'exploitation basé sur Linux.
> 
    Nous sommes fiers d'utiliser le noyau Linux comme base de notre système d'exploitation. L'utilisation de Linux nous permet de maximiser la compatibilité matérielle, de prendre en charge rapidement de nouveaux appareils et de nouvelles fonctionnalités et, si nécessaire, de participer au projet Linux pour le bénéfice de nos utilisateurs.
> 
> Bryan : Pour aller de l'avant, nous voulons définir clairement nos rôles et responsabilités. Nous travaillerons également sur une page dédiée à l'équipe sur notre site web afin que vous puissiez en apprendre un peu plus sur nous, obtenir des informations sur nos rôles dans le projet, ainsi que les moyens de nous contacter.
> 
> Les différents rôles du projet sont actuellement répartis entre les trois membres actifs de l'équipe Solus : Joshua, Peter et moi-même.
> 
> À l'avenir, j'assumerai le titre de responsable technique. Ce rôle implique (mais n'est pas limité à) ce qui suit :
> 
    Développement, activation et support des pilotes et du noyau
    Développement, habilitation et soutien de sous-systèmes clés tels que dbus et systemd
    Développement de divers outils liés à l'emballage et au développement.
> 
> Peter prendra le titre de responsable de la performance. Ce rôle implique (mais n'est pas limité à) ce qui suit :
> 
    Développement, activation et support de nos différentes chaînes d'outils de compilation supportées
    Développement de différentes optimisations de temps de montage
    Développement de divers outils liés au benchmarking.
> 
> Joshua : Je prendrai le titre de responsable de l'expérience. Ce rôle implique (mais n'est pas limité à) ce qui suit :
> 
    Développement, activation et support des environnements de bureau Budgie et GNOME.
    Élaboration et soutien de diverses plateformes et outils liés à l'engagement communautaire, à l'internationalisation et à l'accessibilité.
    Rôle continu de l'engagement communautaire dans le blogue et les comptes rendus des médias sociaux.
> 
> Bryan : J'aimerais profiter de l'occasion pour parler directement à Ikey, puisque je n'ai pas d'autre moyen de le faire à l'heure actuelle et par respect pour ses nombreuses années de service, j'ai le sentiment de lui devoir beaucoup et beaucoup plus.
> 
> Ikey. Vous êtes la raison pour laquelle Solus existe aujourd'hui. Il n'y a pas un seul d'entre nous qui puisse revendiquer cela ou qui ne vous soit pas reconnaissant pour tout ce que vous avez accompli et pour nous avoir permis de faire partie de ce voyage. C'est notre intention sincère de continuer avec Solus d'une manière qui correspond à votre vision du projet et qui reflète les milliers de conversations que nous avons eues au fil des ans sur ce qu'est Solus et ce qu'il doit être. Il ne s'agit en aucun cas d'une forme de prise de contrôle ou de coup d'État hostile. Si, à un moment ou à un autre, vous souhaitez revenir au projet, nous serons heureux de vous accueillir de nouveau. Nous respecterons également votre décision de ne pas le faire si c'est vraiment ce que vous désirez. En attendant, Solus a maintenant une vie qui lui est propre et nous sommes tous profondément investis dans la poursuite de son développement et, pardon pour le jeu de mots, de son évolution.
> 
> Sur une note personnelle, je suis extrêmement reconnaissant pour tout le temps que nous avons passé ensemble sur Solus. Vous m'avez appris beaucoup de choses ces deux dernières années et je garderai ces souvenirs pour le reste de ma vie. C'est un honneur pour moi de pouvoir vous appeler ami. Pour vous, je vous souhaite le plus grand des succès dans tout ce que vous choisirez de faire ensuite dans la vie et que cela vous apporte le bonheur que vous méritez si légitimement. Merci et bonne chance !

Maintenant, Joshua et Bryan nous parlent de l'avenir de Solus et la version 4 fait partie des objectifs malgré ce que peuvent raconter les mauvaises langues.

> Joshua : Ces dernières semaines ont mis l'accent sur la nécessité d'éliminer complètement tous les aspects du facteur bus du projet Solus, dès que nous évoquons ou commençons à utiliser de nouveaux services. Cela va de l'administration du domaine à l'accès aux fonds. Nous avons déjà fait ce qui suit :
> 
    J'ai activé les capacités d'administration pour le domaine GetSolus pour Bryan, de sorte que tout changement de DNS nécessaire peut être fait par lui.
    Pour notre organisation GetSolus GitHub, chaque membre de l'équipe centrale a des droits de propriété.
    Bryan, Peter et moi-même avons tous un accès SSH aux serveurs hébergés et aux machines virtuelles utilisés par Solus.
    Bryan a un accès administratif pour le compte GetSolus SendGrid via la fonction Teammates.
    Comme c'était le cas auparavant avec Phabricator, notre Development Tracker, Bryan, Peter, et moi-même avons tous des capacités administratives ainsi que la possibilité d'effectuer des modifications sur Phabricator via son CLI sur SSH.
> 
> Bryan : L'un des plus grands défis de tout projet open-source est d'aborder le côté juridique de la maison, surtout lorsque les développeurs proviennent de plusieurs pays et juridictions différents. Dans le passé, nous avons géré ce problème en confiant à Ikey la responsabilité financière du projet et en demandant à toute l'équipe centrale de partager le fardeau juridique de choses comme l'application de nos licences et le respect des conditions de licence des logiciels que nous fournissons. Cependant, avec la croissance du projet, il est clair pour nous depuis plus d'un an maintenant que nous devrons finalement traiter Solus comme une entreprise ou compter sur une autre organisation pour devenir notre " centre d'échange " pour les questions juridiques.
> 
> À l'heure actuelle, aucun des membres de l'équipe centrale n'est en mesure d'assumer la responsabilité individuelle d'établir Solus en tant qu'entité juridique, de sorte que nous prévoyons d'adhérer à une entité juridique à but non lucratif. Nous nous préparons à soumettre une candidature officielle pour que Solus rejoigne le Software Freedom Conservancy. The Conservancy est une organisation à but non lucratif qui existe dans le seul but de servir d'organisation mère pour les projets de logiciels libres. Dans ce rôle, ils sont en mesure de le faire :
> 
    Recevez des dons de bienfaisance déductibles d'impôt qui peuvent être versés au projet Solus.
    Nous donner la possibilité d'embaucher des développeurs à temps plein à titre de travailleurs contractuels
    Nous fournir une représentation juridique pour l'application de la licence (droit d'auteur) et de l'image de marque (marque de commerce).
    Aider à l'organisation et au fonctionnement des événements communautaires Solus (p. ex. conférences et réunions)
> 
> Pour ceux d'entre vous qui ont des réserves à l'idée que nous puissions joindre nos forces à celles d'une autre organisation, j'aimerais vous mettre à l'aise en soulignant que le Conservatoire assure la représentation juridique :
> 
    Coreboot
    Projet d'agrégation des droits d'auteur de Debian
    Git
    Godot
    Inkscape
    Mercure
    UEMOA
    Samba
    Vin
> 
> Joshua : Solus a longtemps été défini comme un système d'exploitation à diffusion continue. Ce modèle nous permet de fournir rapidement de nouvelles mises à jour à nos projets ou à nos piles de logiciels pertinents sans sacrifier la stabilité globale de l'expérience utilisateur.
> 
> Dans le passé, nous avons alterné entre les versions majeures et les snapshots ISO. À l'avenir, nous disposerons d'un modèle qui nous permettra de publier de nouvelles versions de Solus tous les mois tout en laissant suffisamment de temps pour que les grands projets arrivent à maturité avant de les livrer.
> 
> Ce modèle aura des versions majeures et mineures. En commençant par Solus 4, qui est notre prochaine version majeure, nous allons mettre en œuvre ce nouveau modèle. Pour Solus 4, notre objectif principal pour le moment est la finalisation de Budgie 10.5 et le report du Software Center à une version 4.x ultérieure. Notre décision de le faire est aussi une reconnaissance du fait que nous avons eu une multitude de mises à jour de pile depuis même notre Solus 3 ISO Refresh, tel que X.Org 1.20.x, et ce sont des changements que nous voulons obtenir dans une nouvelle ISO.
> 
> Pour Solus 4.1, l'accent sera mis sur Budgie 10.5.1 et la mise à niveau de notre pile GNOME vers 3.30.x. Pendant ce temps, nous analyserons le code "xng" ou "prochaine génération" du Software Center et commencerons à travailler à son achèvement. Nous n'avons pas de date ou de sortie fixée, simplement "quand il sera prêt".
> 
> Au-delà de nos communiqués, nous voulons aussi prendre le temps de nous concentrer sur l'amélioration de nos services de première ligne et l'amélioration de la communication avec notre communauté. C'est ce que nous avons détaillé dans notre blogue Improving Community Engagement (Améliorer l'engagement communautaire), ainsi que dans notre article de synthèse suivant, "Shiny Delights". Nous voulons donner de la place à plus de voix dans notre communauté, quelle que soit leur langue, et fournir de meilleurs outils pour les servir.
> 
De plus, Bryan se concentrera sur l'amélioration de nos outils de construction, y compris les améliorations continues et les mises à jour de cuppa et eopkg-deps. Il est également en train de planifier la prochaine version majeure de ypkg.
> 
> Ce n'est qu'un début et un aperçu à court terme. Il y a d'innombrables autres aspects de Solus que nous voulons améliorer ou ré-architecturer, qu'il s'agisse de sol, d'un nouvel installateur, d'un remplacement de la gestion de l'alimentation pour TLP, et oui même d'une Budgie 11 basée sur GTK4.
> 
> Nous comprenons parfaitement que tout ce dont nous venons de parler est très important. Nous espérons que tout cela a fourni une explication complète de notre point de vue. Comme toujours, nous voulons être francs avec vous, notre communauté, au sujet des événements actuels et de la situation de Solus, tout en respectant la vie privée des autres. Si vous avez des questions, nous y répondrons au mieux de nos capacités.

Bravo à l'équipe d'avoir écrit ce billet. On lisait pas mal de bêtises sur le net sur une prochaine mort du projet Solus. Malgré cet évènemennt avec Ikey (j'espère vraiment qu'il va mieux au passage), la team reste soudée et va de l'avant. Solus devrait bientôt devenir une organisation avec un statut juridique. On aura même droit à des versions majeures et des versions mineures (une fois par mois normalement). Bref plein de bonnes chose prévues, vive Solus !

<div id="commento"></div>
<script src="https://cdn.commento.io/js/commento.js"></script>
