---
title : "Nouvelles de Février 2018"
date : "2018-02-08T07:44:00+02:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux"]
categories : ["linux"]
---

Chez elementary ça recommence à parler de Juno, qui devrait sortir forcément après Ubuntu 18.04, donc je tablerai bien sur septembre 2018.

En plus ce ne sera pas la 0.5 mais 5.0, ce qui fait moins "bêta test" avec le zéro qui précède et donc plus "sérieux", Sous a opté pour le même style de numérotation au passage.

Pour Solus, je vous avais dit que Pingax packageait de manière non-officiel le bureau XFCE, il vient de l'annoncer sur son [blog](https://www.reddit.com/r/SolusProject/comments/7k8l23/anyone_dual_booting_on_an_imac/).

On [se rapproche petit à petit de Solus 4](https://solus-project.com/2018/02/07/mate-plasma-and-python-3/), python a été mis à jour, Ikey [a mis à jour Mate en 1.20](https://plus.google.com/b/109348840800096254191/+Solus-Project/posts/9vkHoyTxST9) sur unstable, j'ai aussi aperçu des modifs dans Budgie (mise à jour mineure 10.4.1 ?) et mise à jour de Plasma (de nouvelles iso sont disponibles pour les donateurs). Notez que l'équipe a optimiser le temps de boot, AppArmor qui prenait 1.3 seconde ne prend plus que 8 millisecondes d'après les tests (j'ai hâte de voir cela à la prochaine synchronisation du dépôt stable). Allez, on peut miser sur Février ou au pire Mars pour la sortie de Solus 4.





