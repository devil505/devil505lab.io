---
title : "[Alors on regarde ?] Black Lightning"
tags : ["séries TV"]
categories : ["séries TV"]
---

La chaîne CW a lancée il y a peu une nouvelle série de super héros. Après Arrow, Flash, Legends of Tomorrow et la récupération de Supergirl, voici Black Lightning. Perso, je connaissais pas du tout ce héros. Le pitch est un peu plus original que d'habitude, car là il s'agit d'un ancien super héro qui reprend du service. En effet, Jefferson Pierce alias Black Lightning qui dispose de pouvoir électrique a mis fin à ses activités il y a plusieurs années car c'était physiquement trop intense et dure à supporter pour son épouse. Du coup, il se consacre à sa famille et à son boulot de Principal de lycée. Forcément, des évènements vont le forcer à reprendre du service.

<iframe width="560" height="315" src="https://www.youtube.com/embed/uoUlGwYa4QE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Black Lightning surfe un peu sur la vague de Black Panther. Les protagonistes principaux de la série sont des afro-américains et franchement ca change un peu. La communauté noire est mise en avant avec les bons et les mauvais cotés. L'ambiance musicale plutôt RAP américain colle assez bien. Il n'est pas uniquement question de gangs, la série peut par moment avoir une touche un peu politique comme dans un épisode où des manifestants sont présents devant une statue d'un général sudiste.

L'acteur principal campe bien le personnage de Jefferson Pierce parfois partager entre son rôle de Principal qui éduque ses étudiants à coup de mantra ou de phrases de Martin Luther King et son côté Black Lightning qui désire se venger pour des raisons que je ne dirais pas (pour ne pas spoiler!). Je ne m'étalerai pas non plus sur les deux filles de Jefferson, Anissa et Jennifer, qui sont très présentes à l'écran et qui vont le rester car on apprend rapidement qu'elles ont un point commun avec leur père.

<iframe width="560" height="315" src="https://www.youtube.com/embed/-oPR_TpNkVM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Mention spéciale pour le personnage de Gambi qui au début me paraissait comme une sorte d'Alfred de Batman (oui Black Lightning a sa BatCave) mais avec d'autres talents comme le piratage informatique ou la conception d'armement. Finalement, son personnage est plus profond que cela avec son passé mystérieux.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZBAu4j1Fa0E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Tobias, l'albinos sadique et effrayant, semble devenir le grand méchant idéal de la saison.

<iframe width="560" height="315" src="https://www.youtube.com/embed/mapDXHeBOKs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Je n'ai vu que 10 épisodes pour le moment. Alors, c'est sûr ce n'est pas la qualité des Marvel Netflix mais celle de CW / DC Comics mais ca reste plaisant à regarder.
