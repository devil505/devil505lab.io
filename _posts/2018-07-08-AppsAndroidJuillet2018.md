---
title : "Ma sélection d'applications f-droid pour Juillet 2018"
tags : ["android"]
categories : ["android"]
---

Voici une petite sélection d'applications que vous trouverez sur le store de [f-droid](https://f-droid.org/).

## Lawnchair

Je cherchais un autre lanceur que celui par défaut dans lineageOS et finalement j'ai trouvé [lawnchair](https://f-droid.org/en/packages/ch.deletescape.lawnchair.plah/). Il est basé sur le lanceur du Google Pixel mais avec plus d'options de personnalisation.


## Units

[Units](https://f-droid.org/en/packages/info.staticfree.android.units/) est l'outil parfait pour la conversion d'unités de longueur, superficie, volume, masse, temps, vitesse, température, devise, carburant, stockage, débit binaire, angle, densité, fréquence, débit, accélération, force, pression, moment d'une force, énergie, puissance, viscosité, courant, charge, tension, luminance, éclairement lumineux, radiation et radioactivité (oui y'a vraiment tout cela).

## Shuttle

[Shuttle](https://apt.izzysoft.de/fdroid/index/apk/another.music.player) est le lecteur de musique que j'ai adopté.

## RedReader

Si vous êtes sur le site de news Reddit, pour ma part, j'utilise [RedReader](https://f-droid.org/en/packages/org.quantumbadger.redreader/) comme application dédiée à ce site.

## Aurora Store

Yalp a un concurrent avec [Aurora Store](https://f-droid.org/en/packages/com.dragons.aurora/), qui vous permet aussi de télécharger des APKs depuis le store Google Play, il est aussi possible de renseigner son compte Google pour surement récupérer ses applications déjà payées.

## AnySoft Keyboard

Marre du clavier par défaut de LineageOS ? il y a un clavier alternatif avec plein d'options sur F-Droid, il s'agit d'[AnySoft Keyboard](https://f-droid.org/packages/com.menny.android.anysoftkeyboard/). Pensez à installer le [Pack français](https://f-droid.org/en/packages/com.anysoftkeyboard.languagepack.french/).

On se retrouve le mois prochain pour une nouvelle sélection.
