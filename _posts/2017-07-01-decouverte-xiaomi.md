---
title : "Découverte de XIAOMI"
date : "2017-07-01T16:00:00+01:00"
draft : false
thumbnail : "img/geek.jpg"
toc : true # Optional
tags : ["geekeries"]
categories : ["geekeries"]
---


Il y a deux semaines mon smartphone 1+ One m'a fait le coup de la mort subite. J'ai tout essayé, rien à faire, même après deux ans et demi de bons et loyaux services, ça faisait quand même chier...

Du coup en faisant quelques recherches, j'ai éprouvé quelques difficultés à trouver un bon remplaçant et de surcroît pas cher. Alors bien sûr, j'aurais pu reprendre un 1+, d'ailleurs, leur modèle Five est tout juste sorti mais le hic c'est le prix. La marque chinoise a augmenté les prix à chaque nouveau modèle, à l'époque du One, 350€ c'était parfait niveau rapport qualité prix.

Puis étant utilisateur du site dealabs qui répertories de bons plans achats sur le web, je suis tombé sur [un plan pour Xiaomi Mi 5 à 177€](https://www.dealabs.com/bons-plans/smartphone-515-xiaomi-mi5-blanc---snapdragon-820-ram-3-go-rom-64-go-sans-b20--/370253?page=1). Les specs sont pas trop mal et en plus on est sous la barre des 200€, c'est tentant, pas besoin de mettre 600€ sur un téléphone qui peut s’abîmer par ma faute (chute) ou juste tomber en panne tout seul.

Je connaissais déjà Xiaomi, une marque chinoise qui n'existe pas (encore) en France mais qui fait du bon matos, j'avais déjà acheté des écouteurs et une batterie externe Powerbank de 10400 mAh de cette marque.

Bien sûr, commander un truc de près de 200€ à l'import on peut trouver cela risqué, j'ai commencé d'abord par bien me renseigner et je suis tombé sur des sites instructifs en particulier pour passer commande sur le fameux site de vente Gearbest:

- Rosty les bon tuyaux: <http://rostylesbonstuyaux.fr/>
- Minimachines: <http://www.minimachines.net/category/promos-et-sorties>
- Planète Numérique: <http://www.planetenumerique.com/>

Plus rassuré, j'ai donc fini par commander le Mi 5. Pour la livraison j'ai pris celle en standard avec SingPost même si d'autres recommande France Express. Dans les deux cas, les risques de contrôle par la douane sont ultra faibles. Et oui, les prix sur des sites comme Gearbest sont compétitifs car c'est du Hors-Taxe.

J'ai reçu mon précieux une dizaine de jour après et je ne suis pas déçu. En plus de pas avoir eu des soucis de livraison, je suis ravi par l'appareil. De plus, ils ont même ajouté un adaptateur pour la prise du chargeur (à la base c'est une prise américaine). L'OS installé est MIUI, un Android avec une surcouche de Xiaomi mais offrant une apparence plus sympathique et diverses applications, la rom est internationale donc la langue française était incluse d'office.

La marque chinoise semble intéressante et innovante. Sur dealabs, je me suis laissé tenté par [le bracelet connecté](https://www.dealabs.com/bons-plans/bracelet-connect-xiaomi-mi-band-2---noir-/374183?page=1). Xiaomi fait également un ultraportable de 13" qui me fait de l’œil et présenté [ici par minimachines](http://www.minimachines.net/promos-et-sorties/xiaomi-notebook-air-13-47332), j'y verrais bien installé une distribution linux comme Solus. J'aurais temps d'y repenser à la rentrée et le prix aura peut être même baissé.

N'hésitez par à visiter les liens plus haut et si vous vous aussi vous tenter l'expérience de commander du matos Xiaomi, n'hésitez par à me faire part de votre expérience dans les commentaires. ;-)
