---
title : "Altered Carbon"
tags : ["séries TV"]
categories : ["séries TV"]
---

Altered Carbon est la dernière série SF de Netflix (tirée du roman de Richard K. Morgan)  et c'est du lourd !

<iframe width="560" height="315" src="https://www.youtube.com/embed/dhFM8akm9a4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Le pitch, nous sommes dans le futur où la conscience humaine est stockée dans une pile (en carbon modifié, d'où le titre de la série) située à peu près au niveau de la nuque. Par conséquent, le corps humain devient une enveloppe par mis d'autres. La société est divisée entre les pauvres qui vivent près du sol et qui n'ont pas les moyens d' s'offrir une enveloppe s'ils perdent la vie, et les riches (nommés les maths) , qui vivent dans des tours et qui donc vivent éternellement en changeant d'enveloppe. Après avoir eu sa conscience en suspension durant 250 ans (comprenez par là, peine d'emprisonnement), Takeshi Kovacs se retrouve dans un nouveau corps et est engagé par le riche et puissant Laurens Bancroft afin de découvrir qui a tué ce dernier ou plutôt tenté de tuer, car Bancroft est suffisamment riche pour avoir sa conscience sauvegardée toutes les 48h. Comme son meurtre à été exécuté avant sa sauvegarde, il ne sait pas qui l'a tué. De plus Takeshi Kovacs n'est pas n'importe qui, c'est un ancien membre d'une organisation rebelle (ou terroriste pour d'autres) aujourd'hui disparu.

<iframe width="560" height="315" src="https://www.youtube.com/embed/lSDiwQlV59w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Dans un univers vraiment proche de Blade Runner (on retrouve les bas quartiers, les publicités holographique et les voitures volantes), l'histoire mélange flash-backs du héro, scènes d'action, réalité virtuelle, enquête et complot. Kovacs crèche dans un hôtel géré par une intelligence artificielle nommée Edgar A. Poe, bref, on a tout les ingrédients pour de la bonne SF. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/EtldEVFO04s" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Le rôle principal est tenu par Joel Kinnamen qui jouait le candidat républicain dans House of Cards. Bancroft est joué par l'excellent James Purefoy qui a fait ses preuves depuis longtemps. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/921i8AUZLrc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

C'est je pense l'une des meilleures séries SF produites. Kovacs et sa petite équipe forme des personnages attachants et leurs ennemis sont haïssables comme il faut. Les décors sont franchement pas mal pour une série comme par exemple le Golden Gate en ghetto. Le thème des riches et cupides humains qui se prennent pour des dieux et se permettent tout car ils jouissent d'une certaine forme d'immortalité est assez original. À voir !

