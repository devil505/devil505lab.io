---
title : "Les développeurs prennent-ils les utilisateurs pour des cons ?"
date : "2017-01-22T11:10:03+01:00"
draft : false
thumbnail : "img/edito.jpg"
toc : true # Optional
tags : ["coup de gueule"]
categories : ["editos"]
---


C'est ce que pense Adrien de Linuxtricks:

<iframe width="560" height="315" src="https://www.youtube.com/embed/GuHYuev2K_A" frameborder="0" allowfullscreen></iframe>

Il a tenté de proposer Vivaldi en demande de paquet chez la distro Mageia et il s'est pris une réponse du genre qu'il pouvait apprendre à le packager pour ensuite maintenir le paquet. Je sais qu'une telle réponse peut choquer l'utilisateur lambda. J'ai été utilisateur puis mainteneur de paquets pour Frugalware Linux, je sais qu'il y a plusieurs raisons à une telle réponse.

La première raison est le nombre de mainteneurs de paquets, je ne sais pas combien ils sont chez Mageia mais s'ils sont peu, cela peut être un frein à une demande de paquet.

Une autre raison est qu'il est préférable que le mainteneur utilise l'application qu'il projette d'empaqueter (et oui, ça aide). Vivaldi commence à être connu mais il n'est pas encore aussi populaire dans les applications linux comme l'est Firefox, Gimp ou Libreoffice. Là aussi la popularité du projet est un facteur à ne pas négliger quand on demande un paquet.

Le développeur a donc répondu à l'utilisateur de le packager. Quand j'utilisais Frugalware, j'aimais beaucoup cette distro et j'avais donc une certaine motivation à essayer de faire des paquets. Je suis rapidement devenu mainteneur et j'ai eu à gérer pas mal de paquets, même si je le faisais par plaisir, cela pouvait demander pas mal d'effort et de temps (surtout quand les compilations n'aboutissaient pas et qu'il fallait trouver la raison du problème). Et pour rappel, maintenir des paquets est une activité purement bénévole (ou alors j'ai été dupé), certains utilisateurs ont tendance à l'oublier.

La motivation à vouloir aider la distro que l'on utilise est un facteur important à mon humble avis. Cela dit Adrien n'utilise pas Mageia et faisait la demande de paquet pour quelqu'un d'autre, donc franchement, il n'y avait pas de quoi s'énerver.

À la fin de la vidéo, on a droit à des sortes de menaces pour un futur test de Mageia, bon je n'ai rien contre les testeurs de distributions, comme linux représente à peu près 5% du marché, je doute que ce genre d'articles (en plus destinés à des utilisateurs francophones) puissent avoir un réel impact décisif...ce n'est juste que mon avis hein.
