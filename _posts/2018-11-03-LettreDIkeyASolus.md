---
title : "Lettre d'Ikey à Solus"
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---

Ikey Doherty a envoyé [une lettre à Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Solus-Open-Letter) destinée à la communauté de Solus, la voici traduite par deepl:

> J'aimerais tout d'abord remercier l'équipe Solus pour son travail acharné et sa passion au fil des ans. En guise de réponse à leur récent billet de blog, je ne vois pas du tout ce qu'ils ont fait comme une "prise de contrôle hostile", mais plutôt une évolution naturelle du projet.
> 
> La vérité, c'est que le projet Solus est resté longtemps sur ses propres mérites et pieds, et qu'il a évolué sous sa propre impulsion. Pendant longtemps, j'avais dit que j'étais simplement le premier colon de la ville qui allait devenir Solus, et qu'avec le temps il aurait besoin de ses propres architectes, urbanistes et maire.
> 
> Bien que ce ne soit pas une "rupture nette" pour moi, je peux seulement dire qu'il y a des circonstances atténuantes pour expliquer pourquoi cela s'est produit, pendant certains des moments les plus difficiles de ma vie. Inutile de dire que je n'ai pas accès à plusieurs de mes anciens comptes, ce qui n'est pas quelque chose que je peux rectifier. Cependant, j'ai envoyé des instructions par courriel à Josh afin que l'équipe Solus puisse reprendre possession du nom de domaine, comme dernier geste.
> 
> Si l'on regarde l'analyse chronologique de l'équipe Solus, il est clair qu'ils sont plus passionnés par le projet que quiconque et qu'ils survivront à tout. C'est pour cette raison que j'approuve volontiers leur leadership dans le projet et que je cède tous les droits intellectuels, de dénomination et de marque relatifs à la propriété de Solus à leur collectif avec effet immédiat et permanent, en les reconnaissant comme les propriétaires et dirigeants officiels du projet.
> 
> Solus est un projet construit sur une vision partagée et non sur la voix d'une seule personne. L'équipe et la communauté ont prouvé leur valeur à maintes reprises, dans leur poursuite de l'excellence technique d'une manière non compromettante. J'applaudis leurs efforts en ces temps difficiles et j'ai hâte de voir où ils iront ensuite. Solus est un projet qui nous survivra tous, un hommage de statut immortel construit par passion et amitié.
> 
> En guise de dernière requête à l'équipe Solus, de la part d'une personne qui se considère comme un ami, je leur demande de rester forts à leur cause, et de s'élever au-dessus de la toxicité et de la politique qui affligent le monde du bureau Linux, pour rester une lueur d'espoir, un exemple pour voir comment le logiciel libre devrait être. Ne craignez jamais d'avoir tort, et n'ayez pas peur de réfléchir ; Solus se tiendra la tête et les épaules au-dessus de la foule pour toujours.
> 
> Je suis fier de vous tous - et il est presque certain que nous nous reverrons dans un futur proche - GitHub n'est pas si grand, après tout. Cependant, il est peu probable que je cherche à participer à un projet personnel de quelque nature que ce soit - en tant que nouveau parent, je dois planifier d'être le père de mon enfant et de subvenir aux besoins de ma famille par mon travail, au lieu d'être le père de mon travail et de compter sur mon soutien familial pour me soutenir.
> 
> Pour tous les autres, c'est une période incroyablement excitante dans un paysage en évolution rapide. Solus a résolu une fois pour toutes le problème du facteur bus et est libéré de la "dictature", ayant survécu à une vie très, très dure jusqu'à présent (Remember evolveOS ?) - et sera toujours, toujours, toujours, dur face aux défis et à l'adversité. Solus s'appuie sur la promesse d'excellence technique et d'expérience utilisateur exceptionnelle, et possède un excellent leadership et une vision à toute épreuve pour rendre cela possible. L'équipe a déjà travaillé dur pour vous depuis si longtemps, et continuera de le faire. Mettez votre foi dans la résilience.
> 
> Ikey Doherty (Ami de Solus)

On apprend donc qu'Ikey va bien mais qu'il ne reviendra pas au sein de l'équipe de développement de Solus, beaucoup de changement dans sa vie comme le fait de devenir père l'oblige donc à se concentrer sur d'autres objectifs. Il laisse sa création à l'équipe restante en qui il a pleinement confiance pour continuer à la maintenir et la faire évoluer.

Ikey Doherty était une figure emblématique de Solus mais je tiens à rappeler que Solus n'est pas abandonnée, la preuve nous avons dans l'équipe actuelle:

- [JoshStrobl](https://mastodon.cloud/@JoshStrobl) qui maintient GNOME, qui a commencé à améliorer Budgie et qui assure la partie communication (blog, résaux sociaux) de Solus. Pour moi, c'est un peu lui qui remplace Ikey, en particulier pour le développement de Budgie.
- DataDrake, un peu touche-à-tout, il est en charge du maintient de choses techniques comme par exemple des mise à jour du kernel avec les pilotes qui vont avec.
- Sunnyflunk, si vous avez un bureau Plasma à jour, cherchez pas, c'est grâce à lui !
- Joebonrichie qui maintient pas mal de paquets
- [kyrios](https://mamot.fr/@kyrios) qui est surement le contributeur numéro 1 en mise à jour de paquets

Au passage, j'en profite pour rappeler qu'un hackfest aura lieu ce samedi à 14h UTC, vous pourrez voir Josh bosser sur Budgie 10.5 et DataDrake faire des trucs GTK:

<iframe width="560" height="315" src="https://www.youtube.com/embed/a7qMJvRr0qg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
