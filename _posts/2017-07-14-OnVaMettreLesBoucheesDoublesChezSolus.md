---
title : "On va mettre les bouchées doubles chez Solus"
date : "2017-07-14T08:50:00+02:00"
draft : false
thumbnail : "img/solus.jpg"
toc : true # Optional
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---


## Un employé à plein temps

Voilà on y est, Ikey a quitté Intel et travaille exclusivement pour Solus depuis hier. Son salaire sera pris sur les dons du [Patreon de Solus](https://www.patreon.com/solus). J'espère cela va rendre possible quelques projets rester au point mort.

## Encore une ISO de test

Les donateurs Patreon vont avoir droit [à une nouvelle ISO de test](https://www.patreon.com/posts/12948966) qui corrige des bugs de budgie, apportent des changements visuels et un petit nettoyage de l'installateur.

## Nouvelles fonctionalités pour Budgie

Ca bouge pas mal sur [le github de Budgie Desktop](https://github.com/budgie-desktop/budgie-desktop/commits/master). D'ailleurs le site OMGUbuntu a fait [un article sur 7 nouveautés et améliorations qui arrivent sur Budgie](http://www.omgubuntu.co.uk/2017/07/budgie-desktop-next-release-features). On devrait donc avoir bientôt:

* Un panneau dock, donc plus besoin d'une application tierce style Plank.
* La possibilité de rajouter des panneaux verticaux et non plus seulement horizontaux.
* Avoir la transparence des panneaux avec 3 options: jamais (donc pas de transparence), dynamique (transparence quand une fenêtre est agrandie au max) ou transparence en permanence.
* L'intellihide, en gros, un panneau peut se masquer de manière à ne pas gêner l’utilisateur
* Une applet pour la luminosité de nuit, et oui, plus besoin d'avoir redshift ou f.lux, l'applet gère directement la luminosité de l'écran pour ne pas fatiguer vos yeux.
* Une application de personnalisation, Ikey avait posté une capture il y a peu, comme gnome-tweaks on pourra changer plein de paramètres.
* Un meilleur menu Budgie avec des améliorations de la fonction de recherche.

Perso, j'ai hâte et je ne dois pas être tout seul :-)
