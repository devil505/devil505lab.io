---
title : "The Man of The High Castle"
date : "2017-01-01T18:09:29+01:00"
draft : false
thumbnail : "img/seriestv.jpg"
toc : true # Optional
tags : ["séries TV"]
categories : ["séries TV"]
---

Il y a quelques temps, Amazon s'est lancé dans la production de série, sûrement pour copier Netflix qui a su trouver le filon des séries de qualité (House of Cards par exemple). Il y a un an, je m'étais laissé tenter par l'une de leur première série, The Man in The High Castle. En français, le Maître du Haut Château, qui est un livre de SF de Philip K. Dick. Le pitch n'est pas trop compliqué. L'histoire se place dans les années 60 mais en supposant que les alliés n'ont jamais gagné la seconde guerre mondiale. Du coup l'amérique du nord est divisée en trois morceaux, à l'ouest les États Japonais du Pacifique, à l'est, les États-Unis du Grand Reich, et entre les deux, une sorte de zone neutre. Le casting et les décors sont vraiment pas mal. Bref j'ai apprécié la saison 1.

<iframe width="560" height="315" src="https://www.youtube.com/embed/wgvSE4-uktQ" frameborder="0" allowfullscreen></iframe>

La saison 2 a été rendue disponible en décembre dernier. Je le reconnais, j'ai rapidement visionner les 10 épisodes. C'est même mieux que la première saison car on entre rapidement dans l'action vu que les personnages principaux ont déjà été introduits. Sans vouloir trop en dévoiler, la tournure des manigances politiques pour mettre en péril les relations entre les nazis et les japonais nous tient en haleine jusqu'au dernier épisode.

<iframe width="560" height="315" src="https://www.youtube.com/embed/sJW2K4KAAOM" frameborder="0" allowfullscreen></iframe>

Bref, à essayer !
