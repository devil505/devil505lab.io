---
title : "OST Daredevil"
tags : ["OSTGasme"]
categories : ["OSTGasme"]
---

Ça faisait longtemps que j'avais pas fait un billet OSTGasme sur le blog. Cette fois, on va s'attarder sur l'OST de la série Daredevil de Netflix et composé par John  Paesano. 

On débute avec le thème principal à la fois mélancolique et envoûtant.

<iframe width="560" height="315" src="https://www.youtube.com/embed/EgJSh2uFbZ8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Et on peut pas passer à côté du thème de Wilson Fisk, lent et calme au début puis qui monte en gravité, cela réflète bien la personalité du personnage. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZZngbIlNKpQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Le morceau _Devil of Hell's Kitchen_ regroupe un peu l'ambiance des scènes d'action. Sombre, puis beaucoup de tension dans la musique.

<iframe width="560" height="315" src="https://www.youtube.com/embed/HceIm4tBSaA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

La saison 2 a fait apparaître un nouveau personnage, le Punisher, qui a eu droit à son thème. Évidemment à l'image du personnage, rapide et brutal. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/ShksDZI_5O0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

_Raindrops_ est le love theme de la série, doux et minimaliste.

<iframe width="560" height="315" src="https://www.youtube.com/embed/LO3ZhVStujA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Même chose avec _Dripping Chilis_:

<iframe width="560" height="315" src="https://www.youtube.com/embed/e-xTExOlY2o" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Le mentor de Matt Murdock, Stick, a aussi droit à son thème avec des sons un peu asiatiques en référence à sa maîtrise des arts martiaux.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XvImc5y37oA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Et on termine avec _Stairway to Hell_, la musique d'ambiance de la scène épique (pratiquement sans coupures) de la descente de l'escalier où Daredevil doit faire face au gang des motards.

<iframe width="560" height="315" src="https://www.youtube.com/embed/JXAqn0Qdz1E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
