---
title : "[Alors on regarde ?] Lost In Space"
tags : ["séries TV"]
categories : ["séries TV"]
---

Netflix s'attaque à une nouvelle série SF, après Altered Carbon, Lost in Space (ou Perdus dans l'Espace) remake du show des années 60 qui fût adapté en film à la fin des années 90.

Le pitch, la Terre est menacée et une partie de la population est sélectionnée pour un programme spatial pour coloniser Alpha du Centaure. La famille Robinson fait partie du programme. Suite à des événements, le vaisseau colonisateur doit évacuer des colons dans des navettes sur la planète la plus proche. La série se focalise sur les Robinsons tentant de survivre sur cette planète assez hostile hostile. Il y aura bien sur des rencontres avec d'autre colons plus ou moins sympa et aussi un mystérieux robot.

<iframe width="560" height="315" src="https://www.youtube.com/embed/fzmM0AB60QQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Il faut avouer que Netflix n'a pas lésiné sur les moyens, visuellement c'est beau, les décors et costumes sont assez réussis. Toby Stephens (Black Sails) et Molly Parker (House of Cards)  incarnent les parent Robinson. La série se veut familiale car les enfants Robinson sont assez mis en valeur en particulier le jeune garçon (Max Jenkins) qui a développé une amitié avec le robot. Mention spéciale pour le personnage du Dr Smith pour le coté folle dingue machiavélique et à celui de Don West pour le coté humour.

<iframe width="560" height="315" src="https://www.youtube.com/embed/F30WZOowufc?start=7" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Conclusion, une bonne série de science-fiction et d'aventure pour toute la famille à voir. Une saison 2 a été commandée.
