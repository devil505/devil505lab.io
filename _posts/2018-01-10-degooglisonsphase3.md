---
title : "Dégooglisons phase 3"
date : "2018-01-10T18:11:00+01:00"
draft : false
thumbnail : "img/edito.jpg"
toc : true # Optional
tags : ["editos"]
categories : ["editos"]
---

Troisième phase de degooglisation. Cette fois, consacrée au lecteur de flux RSS. Ce n'est pas une véritable degooglisation je sais, en effet, Google Reader est mort il y a maintenant quelques années. 

Pendant un moment j'ai erré avec feedly pour finalement finir sur inoreader. Ce dernier étant une société, de temps en temps je me tape de petites pubs pour me suggérer de passer sur un compte payant qui me donnerait tout plein d'avantages.

J'ai trouvé une alternative, une fois encore chez framasoft, il s'agit de framanews. C'est en fait une instance tiny tiny RSS hébergée chez framasoft. Jusque là, pas de soucis, j'ai importé mes abonnements d'inoreader vers framanews.

Comme j'aime bien lire mes flux sur mes supports mobiles, je devais trouver une application Android. Vous vous souvenez j'avais installé F-droid, le store libre et gratuit, bah dessus il y a une application nommée [TTRSS-Reader](https://f-droid.org/en/packages/org.ttrssreader/) qui prend charge les instances tiny tiny RSS parfaitement, avec la synchronisation et tout.

Conclusion, je ne suis plus dépendant d'inoreader et je lis mes flux aussi bien sur framanews via firefox que sur mon smartphone avec TTRSS-Reader.
