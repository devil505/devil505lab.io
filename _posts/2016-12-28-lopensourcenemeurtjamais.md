---
title : "L'opensource ne meurt jamais"
date : "2016-12-28T08:39:17+01:00"
draft : false
thumbnail : "img/edito.jpg"
toc : true # Optional
tags : ["android"]
categories : ["editos"]
---

C'est ce que je me dis avec ce qui se passe avec cyanogenmod. J'ai un smartphone OnePlus One et une tablette LG G Pad 8.4. Pour les deux, j'ai dévérouillé le bootloader pour y installer une ROM alternative.

En matière de ROMs alternatives, il existe 2 choix (peut être trois avec Mui...). Il y a les roms AOSP, basées sur la version épurée (sans surcouche constructeurs) d'Android ou les roms basées sur cyanogenmod qui est dérivé d'Android.

Avec son succès cyanogenmod avait en partie basculé dans le commercial avec la création d'une entité nommée Cyanogen et développant Cyanogen OS en partenariat avec certains constructeurs de smartphones.

On apprenait le 25 décembre que Cyanogen stoppait ses activités. Qu'en est il du sort de cyanogenmod ? Et bien on a pas eu vraiment le temps de s'inquiéter. Le projet cyanogenmod renaîtra sous un autre nom: [Lineage OS](http://lineageos.org/Yes-this-is-us/).

C'est ça le gros avantage de l'opensource, c'est qu'un projet n'est pas toujours abandonné (du moins les gros projet hein...).
