---
title : "The Expanse"
date : "2017-07-13T17:40:00+02:00"
draft : false
thumbnail : "img/seriestv.jpg"
toc : true # Optional
tags : ["séries TV"]
categories : ["séries TV"]
---


Franchement, depuis Battlestar Galactica, Sci-fi ne nous avait pas proposé de séries SF digne de ce nom. D'accord il y a Killjoys ou Dark Matter mais ça reste basique et il y a un goût de réchauffé. Et puis il y a The Expanse. Tiré d'un livre que j'ai commencé à lire, l'adaptation est réussie et va parfois plus loin.

<iframe width="560" height="315" src="https://www.youtube.com/embed/krqqqgixNq8" frameborder="0" allowfullscreen></iframe>

The Expanse c'est de la SF mais avec pas mal de géopolitique ou spatiopolitique dans ce cas. Le système solaire est divisé en deux camps, d'un côté la Terre, vieux monde contrôlé par l'ONU, et de l'autre, Mars, la nation émergente plutôt militariste. Inutile de vous dire que c'est la guerre froide entre les deux. Au milieu, vous avez la ceinture, sorte de regroupement de stations spatiales géantes (colonies) où les _belters_ sont principalement des parias qui rêvent d'indépendance, il y a même une organisation, l'APE dirigée par Fred Johnson, des libérateurs pour certains, des extrémistes pour d'autres. Voilà pour la situation.

L'histoire se déroule sur plusieurs tableaux. Sur la station Cérès, le détective Miller est à la recherche de la jeune Julie Mao qui  disparue sans laisser de traces. Dans l'espace, James Holden et des membres de l'équipage du cargo transporteur de glace _Canterburry_ prennent une navette afin de répondre à un appel de détresse. Toutefois, le cargo se fait détruire par un vaisseau inconnue. Holden et sa team sont récupérés par un vaisseau militaire _Donnager_ de Mars qui est très vite accusé d'être le responsable de la destruction du cargo. À partir de là les tensions dans le système solaire monte d'un échelon. Les _belters_ accusent Mars de tuer les siens, la Terre accuse Mars de déstabiliser la politique de non-agression et bien sûr Mars répond qu'il n'y est pour rien. Sans trop spoiler, la destruction du cargo _Canterbury_ et la disparition de Julie Mao sont étroitement liées, et une grande conspiration se cache derrière tout ça, Holden et Miller vont devoir rapidement faire équipe et tenter de sauver l'humanité.

<iframe width="560" height="315" src="https://www.youtube.com/embed/27JmggM5GGQ" frameborder="0" allowfullscreen></iframe>

Les personnages principaux sont Miller le flic routinier et Holden, le héros avec son équipecomposée de Naomi, ingénieure et atout féminin, Amos, le mécanicien sans émotions et Alex, le pilote et ex-militaire martien, mais il y aussi le personnage de Chrisjen Avasarala, sous-secrétaire adjoint pour les Nations unies, la grande stratège de la série, interprétée par une actrice iranienne charismatique. Ce personnage n'est pas présent dans le premier livre, pour la série, il apporte une autre dimension pour la compréhension politique entre la terre et mars.

<iframe width="560" height="315" src="https://www.youtube.com/embed/kQuTAPWJxNo" frameborder="0" allowfullscreen></iframe>

Ce que j'ai apprécié dans cette série, c'est l'effet de réalisme et notamment pour les technologies futuristes. Par exemple, quand un vaisseau change de trajectoire à pleine vitesse, les pressions sont fortes sur le corps humain, du coup, il y a des sortes d'injection automatisées de drogue pour supporter les douleurs. Un _centurien_ capturé et amené sur terre dans l'épisode pilote souffre de douleurs par la pesanteur terrestre car il es né et a vécu toute sa vie durante dans une station où la pesanteur est plus légère.

La série peut être considérée comme lente au début de la première saison, car les choses se mettent en place. Par la suite, c'est vraiment prenant. La saison 2 est encore mieux car le rythme de croisière est atteint et le scénario promet plein de rebondissements. Les combats spatiaux sont au rendez-vous, du vrai space opera.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XhKWeGXduzs" frameborder="0" allowfullscreen></iframe>

Bref, si vous cherchez une bonne série SF, c'est The Expanse qu'il faut voir !

Petit bonus, il y a même linux au 23ème siècle, [la preuve](http://www.omgubuntu.co.uk/2017/07/linux-23rd-century).
