---
title : "Future Man"
date : "2018-02-27T19:25:00+02:00"
draft : false
thumbnail : "img/seriestv.jpg"
toc : true # Optional
tags : ["séries TV"]
categories : ["séries TV"]
---

Voici une série que je viens de terminer et qui m'a un peu marqué. Future Man est une nouvelle série de la plateforme Hulu (The Handmade's Tale, The Runaways...). Le Pitch ? accrochez-vous. Josh Futterman est agent d'entretien pour une société de recherche contre les maladies. Durant son temps libre, il joue à son jeu préféré, Biotics Wars, réputé impossible à terminer mais il persévère justement à le finir. Et il finit par y arriver quand tout à coup, les deux personnages du jeux, Tiger et Wolf, débarquent du futur dans sa piaule. Le jeu n'était en réalité un simulateur pour dénicher le sauveur de l'humanité (donc Josh) face aux Biotics.

<iframe width="560" height="315" src="https://www.youtube.com/embed/vJaUfpbEYIU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Future Man est une série SF humoristique. Elle utilise le procédé de voyages temporels, par conséquent, attendez-vous à un sacré bordel dans la ligne du temps et donc des moments hilarants. Le rôle principal de Josh est tenu par l'acteur d'Hunger Games. Mention spéciale à Derek Wilson qui interprète Wolf, un soldat complètement déjanté qui finit fan du chanteur des années 80, Corey Heart et grand chef cuisinier.

<iframe width="560" height="315" src="https://www.youtube.com/embed/wPJmKIglQVE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

La sérié a de nombreuses références: Retour vers le futur (forcément), Terminator, Pulp Fiction, Avatar ou encore Top Gun.

<iframe width="560" height="315" src="https://www.youtube.com/embed/lk_Tp_InLyI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

C'est un format court, 13 épisodes mais de 30 minutes. Si vous cherchez une série pas longue pour se détendre (et avec une bande son sympa), vous l'avez trouvée !

<iframe width="560" height="315" src="https://www.youtube.com/embed/LOUEqKJZfbY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

