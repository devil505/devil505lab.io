---
title : "Nouvelles de Juillet 2017"
date : "2017-07-10T09:32:14+02:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux"]
categories : ["linux"]
---

Quelques actualités linuxiennes pour ce mois de juillet.

Chez elementary OS, il y a du mouvement sur [le dépôt du futur installateur](https://github.com/elementary/pantheon-installer). Sur github, pas mal d'applications elementray fleurissent et dont certaines finissent par débarquer sur l'AppCenter, j'ai repéré notemment [Torrential](https://github.com/davidmhewitt/torrential) ou [Messenger](https://github.com/aprilis/messenger).

System76 qui vend des PCs avec linux va sortir sa distro, Pop! OS. Accrochez-vous, c'est en fait une Ubuntu Gnome avec le thème de fenêtres et d'icônes Pop!. Non mais sérieusement. En plus, il me semble que le community manager d'elementary, Cassidy James, bosse chez System76, alors pourquoi ne pas mettre elementary OS sur leur PCs...

Chez Solus, Ikey ne chôme pas avec Budgie Desktop, [la preuve](https://github.com/budgie-desktop/budgie-desktop/commits/master). Pour les donateurs Patreon, des ISOs privées avec Budgie instable sont disponibles. D'après certains, cette version semble même stable. Ikey avec même postée [une capture](https://plus.google.com/b/109348840800096254191/photos/photo/112026213399155142823/6440109429598980370?icm=false&sqid=101371177200990436142&ssid=07c8f6ff-8d86-42e3-847e-565f113b5d32) il y a peu de temps sur Google+. Je pensais que c'était la version 0.11 avec Qt mais non c'est une mise à jour mineure (0.13.1). D'ailleurs Solus Budgie change de thème [pour adopter Adapta-Nokto pour le Gtk et Papirus pour les icônes](https://github.com/solus-project/budgie-desktop-branding/commit/9f61ca18e19ff5c3dc1c66bfce4bf98b6ddeb0f8) et aussi [de fond d'écran par défaut](https://github.com/solus-project/artwork/blob/master/backgrounds/Crags.png). Bref, tout ca devrait arriver dans pas longtemps sur le dépôt stable. Et sinon, plus [que quelques jours avant le travail à plein temps d'Ikey pour Solus](https://dev.solus-project.com/C6), Solus va t'elle passer à la vitesse supérieure ?
