---
title : "Elle court, elle court la Solus 4"
date : "2018-02-24T10:21:00+02:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux"]
categories : ["linux"]
---

Certains l'attendent avec une certaine impatience comme sur [ce groupe Telegram](https://t.me/solusfr) cette Solus 4. Il faudra encore attendre un peu même si la [TODO List](https://dev.solus-project.com/T5010) progresse un peu. Il y a eu des mises à jour de KDE Plasma sur le dépôt unstable, donc il y a de fortes chances que Plasma soit disponible avec Solus 4.

Certains diront que c'est de la dispersion d'effort mais sachez que les développeurs de Solus travaillent aussi sur d'autres projets sympathique pour cet OS. D'abord DataDrake, travaille sur un outil ([dépôt](https://github.com/DataDrake/proc-maps)) pour alléger les ressources. Cela pourrait permettre de faire tourner Solus sur des machines un peu limitées.

Ikey, pour sa part, utilise comme machine principale un ordinateur portable avec ces caractéristiques: 4GB RAM, Intel Core i5-3230M (dual-core, folks) and 750GB HDD.

Pas un foudre de guerre vous me direz et c'est normal, le but est se rapprocher de l'utilisateur lambda pour développer des outils permettant de réduire la consommation et d'augmenter l'autonomie. Et oui, c'est son nouveau projet ([dépôt](https://github.com/solus-project/yokeybob)) dont il recherche un nom d'ailleurs (si vous avez des idée c'est [ici](https://github.com/solus-project/yokeybob/issues/1).

Wait and see. Et vous, si vous utilisez Solus, vous en pensez quoi ?






