---
title : "Xiaomi Mi 5 dégooglisé !"
tags : ["editos"]
categories : ["editos"]
---

Tout comme pour la tablette, j'ai franchis le pas. J'ai fini par installer [LineageOS+MicroG](https://lineage.microg.org/) sur mon téléphone Xiaomi Mi 5.

## Dévérouillage du Bootloader, pas si simple chez Xiaomi....

Pour installer une ROM comme LineageOS, il faut passer par un dévérouillage du bootloader afin de pouvoir installer [TWRP](https://dl.twrp.me/gemini/). Ayant l'habitude avec de précédents smartphones, la procédure n'est pas très compliquée sauf avec Xiaomi. En effet, il est obligatoire de passer par leur logiciel de dévérouillage qui n'existe que sur...Windows. Bon, heureusement, j'avais un ancien laptop Dell avec une partition Windows XP. Après avoir installer les drivers USB qui vont bien, je lance le logiciel, je branche le Mi 5 en mode fastboot via USB et là on me dit qu'il faut attendre 72h ! Et oui, c'est Xiaomi qui vous donne l'autorisation après un certain délai. Avec d'autres fabricants, le dévérouillage est instantané mais pas avec Xiaomi, en plus cela requiert une connection internet et ainsi de se loguer avec son compte Mi. Trois jours après j'ai enfin poursuivre la procédure de dévérouillage.

## Ma config avec LineageOS

J'ai réinstallé à peu de chose près les même applications que sur ma tablette. Pour la gestion de SMS, j'ai opté pour [Silence](https://f-droid.org/en/packages/org.smssecure.smssecure/), c'est un fork de TextSecure mais plus sécurisé pour faire simple. Après pour l'envoi de SMS crypté, il faut bien sûr que vos contacts utilisent Silence (là c'est pas toujours gagné...).

Pour l'appareil photo, j'utilise [Open Camera](https://f-droid.org/en/packages/net.sourceforge.opencamera/), l'application libre de photo avec un tas de fonction dont le support du RAW, si des fois je veux retoucher mes photos avec darktable.

Enfin, pour connecter mon bracelet d'activié Xiaomi Mi Band 2, il y a [GadgetBridge](https://f-droid.org/en/packages/nodomain.freeyourgadget.gadgetbridge/), un projet très actif d'application pour gérer ce genre d'outils. Plus besoin de l'application Mi Fit (et de passer par son compte Mi au passage) pour obtenir ses données.

## Conclusion

Dégooglisation réussie pour mon smartphone Xiaomi Mi 5. À votre tour !

