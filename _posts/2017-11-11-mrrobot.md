---
title : "Mr Robot, un petit chef d'oeuvre !"
date : "2017-11-11T11:22:00+02:00"
draft : false
thumbnail : "img/seriestv.jpg"
toc : true # Optional
tags : ["séries TV"]
categories : ["séries TV"]
---

J'ai débuté la saison 3 de l'excellent **Mr Robot**, une bonne occasion pour en parler sur le blog. Le pitch, la série relate les péripéties d'Elliot Alderson, un jeune employé en sécurité informatique. L'excellent Rami Malek tient le rôle principal du personnage à l'esprit un peu confus (c'est justement la base de la série) qui se permet de parler directement au spectateur.

<iframe width="560" height="315" src="https://www.youtube.com/embed/xIBiJ_SzJTA" frameborder="0" allowfullscreen></iframe>

Elliot vas se retrouver impliqué dans une attaque massive en piratage informatique contre **E(vil) Corp** qui symbolise les grandes multinationales ou le GAFA. Car oui. Elliot est très doué en hacking. On pourra noter l'utilisation dans la série de Linux avec Kali Linux, Linux Mint, XFCE...etc. C'est déjà plus crédible qu'un hacker sous windows.

<iframe width="560" height="315" src="https://www.youtube.com/embed/3zapISqsDwQ" frameborder="0" allowfullscreen></iframe>

Les autres personnages sont également géniaux que ce soit Tyrell et sa femme, Angela l'amie d'Eliot, la sœur d'Elliot ou Christian Slater qui joue un personnage bien particulier (spoiler alert!). Les chinois et les hommes de main de la Dark Army sont également impressionnants par leur coté mystèrieux.

<iframe width="560" height="315" src="https://www.youtube.com/embed/1YtHgwTnxrI" frameborder="0" allowfullscreen></iframe>

Les musiques d'ambiance sont prenantes et certains morceaux sont bien choisies. Les prises de vue par la caméra ne sont pas du tout conventionnelles et cela donne un certain style à la série. La saison 3 donne beaucoup de réponses à des questions émises lors des saisons 1 et 2, de plus, la série flirte avec l'actualité politique aux États-Unis avec l'élection de Donald Trump.

<iframe width="560" height="315" src="https://www.youtube.com/embed/4ZW6Eme9eVY" frameborder="0" allowfullscreen></iframe>

Bref, un petit bijou ! Et fuck society !

<iframe width="560" height="315" src="https://www.youtube.com/embed/Z2pjeZMyYg8" frameborder="0" allowfullscreen></iframe>
