---
title : "I'm back!"
date : "2016-12-27T09:32:45+01:00"
draft : false
thumbnail : "img/helloworld.jpg"
toc : true # Optional
tags : ["editos"]
categories : ["editos"]
---

Certains se rappellent peut être de ça:

<img src="../../img/oldblog.jpeg">

Et oui ! je vais essayé de m'y remettre. Cette fois exit Wordpress, j'utilise Hugo un générateur de pages en Markdown qui est au final assez simple d'utilisation, et le tout hébergé sur Github. Bon OK, j'ai piqué l'idée à [Stéphane Robert](https://stephrobert.github.io/).

### Quoi de neuf depuis ?

Bah toujours sur elementary OS. Je co-gère avec Nikos le site de la communauté francophone qui marche pas trop mal. De plus, la version 0.4 Loki est sortie il y a quelque temps, j'ai posté sur le site ma découverte de cette version en 3 parties ([partie 1](http://www.elementaryos-fr.org/a-decouverte-de-loki-partie-1/), [partie 2](http://www.elementaryos-fr.org/a-decouverte-de-loki-partie-2/) et [partie 3](http://www.elementaryos-fr.org/a-decouverte-de-loki-conclusion/)). Pour résumer, c'est une bonne version (je l'utilise au quotidien) mais qui peut souffrir de quelques petites erreurs de jeunesse.

### Ah et aussi

J'ai aussi récemment découvert une autre distribution tout aussi intéressante: Solus. Un peu à l'image d'elementary OS avec Pantheon, Ikey Doherty, le développeur principal de Solus, a crée un environnement de bureau pour sa distro: Budgie Desktop. On retrouve un peu cette même envie de proposer un bureau à la fois esthétique et épuré, enfin c'est mon avis hein.

### Et sinon ce sera comment ?

Je parlerai principalement de linux et de quelques geekeries (android, films, séries, musique...). Et les billets seront courts, pas de pavés hein, question de style hein :-D
