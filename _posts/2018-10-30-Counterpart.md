---
title : "[Alors on regarde ?] Counterpart"
tags : ["séries TV"]
categories : ["séries TV"]
---

J'ai terminé une série qui a été diffusée, je crois, en début d'année (mieux vaut tard que jamais hein), il s'agit de Counterpart. Counterpart est une série d'espionnage avec un petite touche de fantastique (vous allez comprendre avec le pitch). Justement, le pitch. La série se déroule en Allemagne. Un employé, Howard Silk, d'une agence un peu mystérieuse dont la femme est dans le coma suite à un accident vit sa une vie assez banale. Un jour, il est convoqué par ses supérieurs et se retrouve présenté à son double mais avec une personnalité bien différente de lui. L'agence abrite secrètement un passage, découvert durant la guerre froide, vers un monde parallèle au nôtre. Depuis cette découverte, les deux mondes ont déviés dans leur chronologie. L'agence est comme un passage frontalier avec gardes, file d'attente...etc. Des agents dissidents de _l'autre coté_ se sont infiltrés dans notre monde, ainsi le personnage principal va devoir faire équipe avec son double pour les trouver et déjouer un complot.

<iframe width="560" height="315" src="https://www.youtube.com/embed/c3Bu2DOM66g" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Dans le rôle principal, on retrouve l'excellent JK Simmons. Il y également pas mal d'acteurs anglais que j'avais déju dans d'autres séries. Sans trop spoiler, j'ai adoré la manière dont les deux Howard doivent s'adapter dans le monde de l'autre. Les autres personnages sont également intéressants que ce soit la tueuse à gage, le chef de la sécurité (Ulrich Thomsen qui jouait Proctor dans _Banshee_) ou encore le personnage de Clare (Nazanin Bionadi, aperçue dans _Homeland_).

<iframe width="560" height="315" src="https://www.youtube.com/embed/mwqKdd7PNAA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Si vous cherchez une série d'espionnage avec un brin de science-fiction et quelques rebondissements, foncez ! Une saison 2 est prévue pour bientôt !

<div id="commento"></div>
<script src="https://cdn.commento.io/js/commento.js"></script>