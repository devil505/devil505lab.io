---
title : "Nouvelles de Juin 2017"
date : "2017-06-05T10:18:20+02:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux"]
categories : ["linux"]
---

Que s'est il passé depuis la dernière fois ? :-)

Coté elementary, le nouvel AppCenter est disponible pour les utilisateurs de la 0.4.1 Loki (ou ceux qui mettent à jour leur système). Comme prévu, il met en avant sur sa page d'accueil les applications tierces développées pour elementary à prix libre (gratuit ou mettez le prix que vous voulez). Il commence à y en avoir plusieurs:

- Tomato (application pomodoro)
- NaSC (application de calcul)
- Harvey (pour tester un code de couleur)
- Eddy (pour installer des fichiers .deb)
- Nutty (utilitaire réseaux)
- Translator (pour tarduire des textes)
- Nimbus (applet météo sur le bureau)
- Hourglass (horloge)
- Agenda (gestion de tâches)
- Vocal (pour les podcasts)
- Bookworm (pour lire des eBooks)
- Notes-Up (prise de notes)
- Spice-Up (genre d'équivalent à Powerpoint)

C'est plutôt pas mal. Après il faut voir si dans la durée si les développeurs de ces applications continueront à les développer. J'ai testé quelques unes de ces applications, certaines sont assez intéressantes comme Spice-Up qui permet de créer facilement une présentation. D'un point de vue personnel, vais-je les utiliser ? Non, car elles ne répondent par forcément à un manque ou un besoin pour moi. On verra par la suite comment cette boutique d'applications évolue.

Coté Solus, des éléments de KDE débarquent sur leur dépôt et il est fort possible qu'ils ont trouvé quelqu'un pour maintenir KDE et le bureau Plasma. Vas t-on avoir une nouvelle saveur Solus avec KDE/Plasma après celle pour GNOME ? C'est fort possible ! C'est bien de proposer plusieurs possibilités du moment qu'il y a mainteneur attrité pour chaque élément important du projet.

J'avais packagé luminance-HDR souvenez-vous ? Et bien la plateforme Phabricator de Solus a du succès, ils ont désormais un outil nommé arc qui envoie directement vos commits sur la plateforme pour qu'un développeur puisse passer en revue votre contribution. C'est vraiment pratique. [La preuve](https://dev.solus-project.com/source/luminance-hdr/). Sur Patreon, le projet Solus a dépassé les 1500$ par mois, chapeau !

Je viens de commander un nouveau disque dur, je pense sérieusement à basculer pleinement sur Solus. ;-)

Allez à bientôt !
