---
title : "elementary OS store"
date : "2017-04-07T17:16:16+02:00"
draft : false
thumbnail : "img/elementary.jpg"
toc : true # Optional
tags : ["linux","elementary OS"]
categories : ["elementary OS"]
---

Les choses avancent à grand pas pour l'AppCenter de la prochaine version d'elementary OS.

Philip Scott qui développe une application de présentation, genre PowerPoint, raconte que sur son dashboard de développeur il n'a juste qu'à cliquer sur Build et ensuite son application se retrouve sur l'AppCenter.

<img src="../../img/appcenter-spiceup1.jpeg">

<img src="../../img/appcenter-spiceup2.jpeg">

En plus il a même droit à une sorte de bannière sur la page d'accueil. Donc voilà, ce n'est pas pour rien que l'icône de l'AppCenter aura l'apparence d'un petit magasin. Stay tuned.
