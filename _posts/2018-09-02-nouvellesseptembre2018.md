---
title : "Nouvelles de septembre 2018"
tags : ["linux"]
categories : ["linux"]
---

Billet de nouvelles du mois qui sera consacré exclusivement à Solus.

## Où est Ikey ?

Beaucoup se posent la question et s'inquiètent. Ikey étant le fondateur de Solus et comme il n'a effectué absolument aucunes activités pour le projet depuis un moment, certains se pose des soucis pour l'avenir de Solus. Rassurez-vous, il va bien, c'est juste qu'il est en Angleterre et il n'a pas encore de connection internet convenable et pas chère. De toute façon, il y a d'autres développeurs chez Solus et on vas en parler.

## Nouvelle gestion

Avec Ikey absent, c'est plus ou moins Josh qui a pris les commandes. Il a mis en place des méthodes assez intéressantes. En effet, Solus est une rolling release mais le dépôt est mis à jour une fois par semaine (généralement le vendredi) afin d'éviter au mieux les problèmes. Désormais, Josh établit une tasklist pour la semaine, le but étant de se fixer des objectifs (et les autres développeurs sont bien sûr invités à y ajouter leur propres objectifs), exemple [ici](https://dev.solus-project.com/T6762) ou [là](https://dev.solus-project.com/T6792). En plus de cela, Josh tient informés les utilisateurs en faisant un billet (The Roundup) sur le blog officiel afin de résumer le travail réalisé. Les utilisateurs de Solus n'ont, à mon avis, jamais été aussi bien informés.

## Le driver NVIDIA beta pour bientôt

Je ne sais pas si c'est l'engouement suscité par le support de beaucoup de jeux windows par steamplay/proton mais les développeurs ont finalement cédés à la demande de certains utilisateurs pour avoir le driver NVIDIA beta (396) sur le dépôt. Toutefois, ce sera réservés aux téméraires car il faudra l'installer en ligne de commande ou via le software center car le logiciel DoFlicky qui permet d'installer le driver en un clic ne le proposera pas (car considéré comme beta).

## Quoi pour la suite ?

Petite indiscrétion de la part de Kyrios, l'équipe devrait bientôt bosser sur la mise à jour de X.org et de python 3. DataDrake voudrait également mettre à jour LLVM et Mesa pour améliorer le support de Steamplay.

Allez, rendez-vous en octobre !

<div id="commento"></div>
<script src="https://cdn.commento.io/js/commento.js"></script>
