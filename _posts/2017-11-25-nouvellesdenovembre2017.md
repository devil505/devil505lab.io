---
title : "Nouvelles de novembre 2017"
date : "2017-11-25T13:24:00+02:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux"]
categories : ["linux"]
---

## L'installateur d'elemenrary OS se refait une beauté

Vous n'êtes pas sans savoir que la team elementary travaille sur son propre installateur. Daniel Foré a posté [quelques captures](https://plus.google.com/+DanielForé/posts/NnugpiEERQ6).

<img src="http://lh3.googleusercontent.com/-5jeNitMIQiM/WhPZXj7JCdI/AAAAAAAANn8/iZOCQ36CAdMgZD6eRam99gG5hTlKVATVgCJoC/w530-h367-n-rw/Screenshot%2Bfrom%2B2017-11-20%2B23.43.48.png">

## Les devs de Solus sont des gamers

L'équipe de Solus a posté une [news](https://solus-project.com/2017/11/12/this-week-in-solus-install-48/) qui prouve que c'est des gamers. En effet, le Linux Steam Intégration dispose de plus d'options de configuration. L'équipe travaille également sur le support de Vulkan sensé remplacer openGL.

<iframe width="560" height="315" src="https://www.youtube.com/embed/K87iDHzwtoc" frameborder="0" allowfullscreen></iframe>

De plus, le snap de LSI est disponible pour Ubuntu 17.10.

<iframe width="560" height="315" src="https://www.youtube.com/embed/8NhHuuS2KQI" frameborder="0" allowfullscreen></iframe>

Solus 4 devrait débarquer ce mois-ci, pensez à surveiller [la todo list](https://dev.solus-project.com/T5010).

Allez, on se revoit en décembre. En attendant, vous pouvez vous taper les 7 heures du dernier hackfest de Solus:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Wo32dMmtaSc" frameborder="0" allowfullscreen></iframe>



