---
title : "Si vous n'avez pas compris c'est normal !"
date : "2017-09-18T18:46:00+02:00"
draft : false
thumbnail : "img/solus.jpg"
toc : true # Optional
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---


Y'a pas à dire chez Solus, la communication ce n'est pas toujours leur fort. Et oui, les développeurs ne sont pas toujours les plus compétents pour parler aux utilisateurs lambda.

## Un Hackfeist cool mais c'est quoi au juste ?

Alors le weekend dernier, l'équipe de Solus a organisé un hackfeist. En fait c'est une sorte de réunion publique de développeurs où ils décident de bosser conjointement sur un projet. On a donc pu les voir sur youtube bosser sur Budgie 0.11 qui utilisera Qt à la pace de Gtk. J'ai juste regarder 5 minutes car c'est assez technique et un peu chiant au final. Voilà le replay si vous voulez vous taper les 6 heures:

<iframe width="560" height="315" src="https://www.youtube.com/embed/J85OBqT51ZM" frameborder="0" allowfullscreen></iframe>

Il fallait être en même temps sur le canal IRC #Solus-LiveStream pour y voir un interêt. Les plus calés en code pouvaient intervenir et aider au projet. J'ai aperçu un FerenOS-Dev ce soir là, comme quoi...

## Ca a l'air sympa mais j'ai rien compris

Le compte youtube de Solus a aussi plublié une autre vidéo pour montrer les avancées de ferryd:

<iframe width="560" height="315" src="https://www.youtube.com/embed/vMxxyDznSq0" frameborder="0" allowfullscreen></iframe>

Beaucoup de gens ont eu la même réaction, en gros ils ont trouvés cela sympa mais ils n'ont rien compris. Ferryd devrait normalement faciliter la vie des développeurs pour la gestion des dépôts et des paquets. Après la vidéo n'est franchement pas parlante pour un utilisateur lambda.

Il va falloir faire un effort chez Solus pour savoir parler ses utilisateurs surtout que c'est une distro visant la population user-friendly du monde linux. C'était mon petit coup de geule du moment.






