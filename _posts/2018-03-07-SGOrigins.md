---
title : "Stargate Origins"
tags : ["séries TV"]
categories : ["séries TV"]
---

Si vous avez plus de trente ans, vous avez peut être eu votre période Stargate, le film, SG-1, la série puis les spin-off de cette dernière. Faute d'audience avec Stargate Universe, la MGM avait décidé de mettre fin à la franchise.

Récemment une web série nommée Stargate Origins a débutée et son possible succès pourrait faire relancer la franchise. Alors est-ce qu'on regarde ?

Bah c'est tout le problème, la série n'est visible que sur internet et via une plateforme payante qui n'est pas accessible aux européens...les possibilité d'un succès sont donc vraiment minces. Si vous voulez voir la série, il faudra passer par des réseaux de piratage... ou en cherchant bien sur YouTube.

Et la web série en elle-même ? Le pitch ne reprend aucune intrigue existantes que ce soit du film ou des séries. En fait, c'est un préquel, elle relate les moments suivants la découverte de la porte en Égypte par les soldats anglais. La porte est étudiée dans un entrepôt par un professeur accompagné de sa fille, seulement voilà, un autre professeur, allemand, avec nettement moins d'éthique (c'est un SS...) débarque avec ses sbires. Ce dernier sait comment la porte fonctionne et l'active pour la traverser pour découvrir un nouveau monde. À partir de là tout s'emballe.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Itqq9hzLapk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

C'est un format court, les épisodes font 10 minutes max et le budget est très limité. Toutefois cela reste plaisant à regarder pour les nostalgiques car on retrouve la porte avec la création du vortex, les faux dieux ou encore le fameux naquada. Pour les raison évoquées plus haut, je doute sincèrement de la reprise de la franchise Stargate de si tôt...
