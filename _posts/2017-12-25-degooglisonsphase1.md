---
title : "Dégooglisons phase 1"
date : "2017-12-25T11:19:00+01:00"
draft : false
thumbnail : "img/edito.jpg"
toc : true # Optional
tags : ["editos"]
categories : ["editos"]
---

En me documentant sur la campagne Dégooglisons l'internet de framasoft, je me suis dit pourquoi pas essayer. Alors débutons par la phase 1, en clair le mail !

Alors OK, j'utilise gmail comme mail principal, tout le monde l'utilise aujourd'hui, hotmail s'est fait dépassé il y a un moment. D'un coté, c'est pratique, vous acheté un billet d'avion, vous recevez la notification par mail, puis l’algorithme de Google le scanne et peut par exemple vous envoyé des notifications sur votre vol. Cool non ? Après si les services Google peuvent lire certains mails et ils peuvent lire les autres, ceux un peu plus privés et çà c'est moins cool.

Y'a t'il une alternative à Gmail ? il y a pas mal de possibilités gratuites (plus ou moins). Même la navigateur Vivaldi propose un webmail gratuit préservant la vie privée, leurs serveurs étant en Islande et les lois là-bas semblent assez respectueuses de la vie privée. Je n'ai pas encore testé mais je le garde dans un coin.

Puis dans Mr.Robot j'ai aperçu ProtonMail, j'en avais déjà entendu parlé. J'ai tenté le coup, c'est plutôt pas mal. Le plan gratuit est assez basique et peut être un brin limité pour un compte mail principal, le plan payant au-dessus de 24€ par an semble plus adéquate. Deux euros par mois, on évite d'aller à la machine à café au boulot et la perte est limitée :-)

<iframe width="560" height="315" src="https://www.youtube.com/embed/CoO04yJuO_w" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>

Il est même recommandé d'avoir l'adresse en .ch plutôt que .com mais on ne peut que l'avoir avec le plan payant. Les emails sont sécurisés de bout à bout, évidemment à 100% si vous communiquez avec une autre personne utilisant protonmail. Il n'y a pas la possibilité de l'utiliser avec un client mail comme thunderbird mais pas grave j'ai pour habitude d'utiliser les interfaces webmail et pour android il y a une application protonmail.

<iframe width="560" height="315" src="https://www.youtube.com/embed/6Ynb5BxWuhk" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>

Conclusion, je n'ai pas totalement migré, j'ai commencé pour certaines newsletter et autres services comme Firefox Sync mais il y a encore boulot. Après lorsque l'on donne son mail à quelqu'un, gmail est plus simple à faire passer que protonmail, vivaldi ou tutanota. On verra plus tard comment évolue la situation. Prochaine phase, l'agenda ;-)
