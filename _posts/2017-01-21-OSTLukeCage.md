---
title : "OST Luke Cage"
date : "2017-01-21T10:29:48+01:00"
draft : false
thumbnail : "img/ostgasme.jpg"
toc : true # Optional
tags : ["OSTGasme"]
categories : ["OSTGasme"]
---

Je débute une nouvelle catégorie pour ce blog: OSTGasme. Catégorie consacrée à mes bonnes découvertes de bandes originales (OST) de films ou séries.

Pour commencer, je vais vous parler de l'OST de la série Netflix et Marvel: Luke Cage. J'avais apprécié la série sortie en septembre 2016 qui relate les péripéties du super héro dans le quartier d'Harlem. La bande son est très Soul et un peu Rap, cela colle ainsi au célèbre quartier de New York où se déroule l'action.

<iframe width="560" height="315" src="https://www.youtube.com/embed/663l_h2EP8Y" frameborder="0" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/qCGOPStmsiQ" frameborder="0" allowfullscreen></iframe>

On ne peut passer à coté du générique du début qui donne le tempo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q6fnq4RJiFE" frameborder="0" allowfullscreen></iframe>

Dans le morceau suivant qui symbolise l'arrivée de Diamondback, l'ennemi juré de Cage, on sent une atmosphère mystérieuse qui devient rapidement inquiétante.

<iframe width="560" height="315" src="https://www.youtube.com/embed/RWK2QuqrB8o" frameborder="0" allowfullscreen></iframe>

Le morceau Street Cleaning réflète bien l'ambiance musicale de la série. C'est le passage où Luke Cage passe à l'action. Il y a presque un petit coté Shaft.

<iframe width="560" height="315" src="https://www.youtube.com/embed/VQvyf-2Fkps" frameborder="0" allowfullscreen></iframe>

Bien qu'assez court, le thème de Cottonsmouth est lui aussi jouissif.

<iframe width="560" height="315" src="https://www.youtube.com/embed/CJG7He0uwQc" frameborder="0" allowfullscreen></iframe>

L'OST dont les compositeurs sont Adrian Younge et Ali Shaheed Muhammad comporte pas moins de 51 pistes, il est donc assez complet. Si vous êtes fan de Soul, je pense qu'il mérite de se le procurer.
