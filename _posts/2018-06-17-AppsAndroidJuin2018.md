---
title : "Ma sélection d'applications f-droid pour Juin 2018"
tags : ["android"]
categories : ["android"]
---

Voici une petite sélection d'applications que vous trouverez sur le store de [f-droid](https://f-droid.org/).

## episodie

[Episodie](https://f-droid.org/packages/pl.hypeapp.episodie/) est une application pour gérer vos visionnages de séries télé.L'application est encore jeune et bugge par moment, toutefois il y a des fonctions intéressantes pour les seriesvores.

## Reader for Pepper &Carrot 

[Reader for Pepper&Carrot](https://f-droid.org/fr/packages/nightlock.peppercarrot/) est une toute nouvelle application du store. C'est un lecteur pour le webcomic libre, opensource et gratuit [Pepper & Carrot](https://www.peppercarrot.com/fr/).

## RadioDroid

Avant avec le Play Store, j'utilisais Tune In pour écouter des stations radio mais avec l'arrivée de la pub...c'est devenu embêtant, pas de soucis car il existe RadioDroid sur f-droid et qui fait exactement la même chose en mieux.

## Exodus Privacy

[Exodus Privac](https://f-droid.org/packages/org.eu.exodus_privacy.exodusprivacy/) est une app qui analyse toutes les applications installées depuis le Google Play Store et vous indique ensuite quel pisteur est installé avec telle application, vous risquez d'avoir des surprises ! Leur base de données s'étoffe petit à petit, n'hésitez pas à consulter [le site officiel](https://exodus-privacy.eu.org/).

## OpenL

Lors de ma dégooglisation, je vous parlais du service de traduction DeepL pour se passer de Google Traduction, et bien il y a une application pour utiliser DeepL, c'est [OpenL](https://f-droid.org/fr/app/com.anthony.deepl.openl).

## Feeel

Vous avez peut être déjà entendu parler de ces applications qui vous permettent de faire des exercices de sport frationnés et durant 7 minutes. Les applications pour cela sur le Google Play Store sont généralement remplies du pub et il faut payer pour avoir accès à davantage de fonctions. Sur f-droid, [Feeel](https://f-droid.org/packages/com.enjoyingfoss.feeel/) fait la même chose sans ce genre de désagrément, l'appli est encore un peu basique mais on espère qu'elle va s'améliorer.

On se retrouve en juillet pour une nouvelle sélection.
