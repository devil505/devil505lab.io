---
title : "Taboo"
date : "2017-01-15T14:33:13+01:00"
draft : false
thumbnail : "img/seriestv.jpg"
toc : true # Optional
tags : ["séries TV"]
categories : ["séries TV"]
---

Petite présentation de Taboo, la série anglaise du moment et qui est le petit bébé de l'acteur Tom Hardy réputé pour s'impliquer mentalement mais aussi physiquement dans les rôles qu'il interprête. Perso, cette série est une réussite !

<iframe width="560" height="315" src="https://www.youtube.com/embed/6nodZGlBPL4" frameborder="0" allowfullscreen></iframe>

Le picth, James Delaney, que tout le monde pensait disparu dans un naufrage en Afrique, ressurgit à Londres pour les funérailles de son père et cela ne vas pas plaire à tout le monde. En effet sa soeur et son mari ne sont pas contents de le revoir mais c'est surtout la compagnie des Indes Orientales, la puissante compagnies qui règne sur les échanges commerciaux. Et oui, James Delaney hérite de la société de son père et d'un bout de terre du coté de la baie Vancouver qui est tant convoité par la compagnies des Indes Orientales car c'est la porte pour des échanges commerciaux avec la Chine. De plus, les relations sont tendues entre la couronne d'Angleterre et les américains, attention poudre à canon en vue !

Tom Hardy interpète bien évidemment James Delaney qui a un côté mystique (pour ne pas dire chamanique), surement hérité de sa mère, amérindienne, morte dans un asile de fous. La reconstitution du Londres de 1814 est vraiment bien foutue. C'est Ridley Scott qui produit également la série, ce n'est pas rien. La distribution d'acteurs est vraiment pas mal aussi, on s'attache en particulier aux alliés de James, le serviteur Brace, le chimiste, l'actrice...etc

<iframe width="560" height="315" src="https://www.youtube.com/embed/W1fiijqrKuc" frameborder="0" allowfullscreen></iframe>

Durant toute cette première saison, James devra éviter les pièges mais il n'en reste pas là, il riposte (on comprend rapidement que notre personnage a un plan).

Comme vous pouvez le constater sur la vidéo suivante, la bande son est tout simplement prenante.

<iframe width="560" height="315" src="https://www.youtube.com/embed/4iMsi0iZweg" frameborder="0" allowfullscreen></iframe>

Bref, il n'y a pas que les américains, les anglais peuvent aussi nous faire de vrais petits bijoux en matière de série :-)

Une saison 2 a déjà été commandée.
