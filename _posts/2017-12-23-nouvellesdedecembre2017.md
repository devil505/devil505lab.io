---
title : "Nouvelles de décembre 2017"
date : "2017-12-23T11:37:00+02:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux"]
categories : ["linux"]
---

## Solus 4 retardé

La solus 4 a été repoussée pour janvier 2018 finalement. D'après ce que m'a dit Kyrios, Ikey se concentre sur LDM, non pas LightDM mais Linux Driver Management qui permettra une reconnaissance à chaud (hotplug) du matériel, bref un peu comme du Plug&Play quoi. Vous branchez une souris Razer, on invite pour installer les drivers qui vont bien apparaitra, idem pour l'imprimante...etc

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">Improved hotplug support coming to Linux Driver Management - it knows things. <a href="https://twitter.com/hashtag/Solus?src=hash&amp;ref_src=twsrc%5Etfw">#Solus</a> <a href="https://twitter.com/hashtag/Linux?src=hash&amp;ref_src=twsrc%5Etfw">#Linux</a> <a href="https://t.co/yQNnW5HqwA">pic.twitter.com/yQNnW5HqwA</a></p>&mdash; Solus (@SolusProject) <a href="https://twitter.com/SolusProject/status/942471598023626753?ref_src=twsrc%5Etfw">17 décembre 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


## Ce qui nous attend pour Solus 4

En plus de LDM, le software center aura le support pour les paquets snap donc adios la partie 3rd party pour un onglet avec un des paquets snaps pour les trucs un peu particulier à packager. Là où pouvait dire qu'il manquait des paquets à Solus, les paquets snap devrait combler certains oublis.

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/Solus?src=hash&amp;ref_src=twsrc%5Etfw">#Solus</a> 4 Coming January 2018 with Snap Support in Software Center, Dynamic Discovery of Hardware Drivers <a href="https://t.co/7UPEZAK7jC">https://t.co/7UPEZAK7jC</a> <a href="https://twitter.com/SolusProject?ref_src=twsrc%5Etfw">@SolusProject</a> <a href="https://twitter.com/snapcraftio?ref_src=twsrc%5Etfw">@snapcraftio</a> <a href="https://twitter.com/hashtag/Linux?src=hash&amp;ref_src=twsrc%5Etfw">#Linux</a> <a href="https://twitter.com/hashtag/opensource?src=hash&amp;ref_src=twsrc%5Etfw">#opensource</a> <a href="https://t.co/baq76TZxkC">pic.twitter.com/baq76TZxkC</a></p>&mdash; Marius Nestor (@MariusNestor) <a href="https://twitter.com/MariusNestor/status/944221902389829632?ref_src=twsrc%5Etfw">22 décembre 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Le kernel 4.14 LTS et Mesa 17.3 (je vous avais dit que les devs étaient des gamers) seront de la partie.

À l'année prochaine ;-)





