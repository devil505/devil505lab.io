---
title : "Nouvelles de Juillet 2018"
tags : ["linux"]
categories : ["linux"]
---

## Solus

Toujours pas de Solus 4 mais le site officiel a posté [une nouvelle](https://solus-project.com/2018/06/26/software-center-progresses/). La prochaine mouture du Solus Software Center avance bien selon les nouvelles captures d'écran de l'article. Budgie 10.5 ne devrait pas tarder non plus. D'ailleurs [le projet Budgie](https://github.com/solus-project/budgie-desktop) revient entièrement sous le giron de Solus.

## elementaryOS

La beta 1 de 5.0 Juno est disponible ! Vous pouvez télécharger l'iso [ici](https://developer.elementary.io/). Je l'ai testé, je reste un peu sur ma faim, il y a des nouveautés mais qui sont assez petites en réalité, du coup, je trouve que la différence entre Loki et Juno ne semble pas vraiment visible, mais ce n'est que mon avis hein.

<iframe width="560" height="315" src="https://www.youtube.com/embed/dl7HxiwfHvQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Un peu de libre

Quelques nouvelles dans le monde du libre. Le moteur [Qwant se refait une beauté](https://blog.qwant.com/fr/qwant-change-son-design/). [Protonmail a été victime d'attaques DDOS](https://www.macg.co/logiciels/2018/07/protonmail-vise-par-une-attaque-ddos-102865), ayant un compte chez eux, j'aimerais bien savoir l'interêt de s'en prendre à eux de la sorte. [PixelFed subit (ou va subir) plein d'améliorations sympas](https://mastodon.social/@pixelfed), faudrait que je l'utilise davantage.

Rendez-vous en août!

