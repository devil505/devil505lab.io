---
title : "Passage à Solus"
date : "2017-06-11T08:47:53+02:00"
draft : false
thumbnail : "img/solus.jpg"
toc : true # Optional
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---

Et voilà, sur ma station de travail, j'ai viré elmentary OS pour y mettre Solus avec Budgie desktop.

![](../../img/bureau_budgie.jpg)

# Budgie versus Pantheon

Budgie regroupe tout par défaut sur la barre horizontale supérieure. À coté de menu des applications, on peut rajouter des raccourcis et l'espace des applications en cours n'affiche que les icones, on gagne donc un peu de place. Pas besoin d'un dock au abs de l'écran comme sous Pantheon. Le panneau Raven est plutôt bien pensé. Budgie n'est n'y plus ni moins qu'une alternative à Gnome shell, du coup les applications par défaut sont celles de Gnome, exemple, pour l'agenda c'est Gnome Agenda. Solus ne s'est pas embeté à ré-écrire de nouvelles applications. Un petit plus, c'est que les comptes en ligne fonctionnent très bien avec Gnome-online-accounts alors que la variante pour Pantheon est encore un peu limitée. Après su un plan esthétique et de design, Pantheon est peut être un cran au-dessus, l'aspect a toujours été primodial pour l'équipe d'elementary.

# Richesse ou pauvreté du dépôt ?

Avec Solus, point de PPAs, donc pas les inconvénients de rajout de dépôts tiers. Toutefois, le dépôt de Solus peut paraitre mince mais l'essentiel est déjà packagé. En plus, la part belle est faîte au jeux et Steam est présent avec une application maison de Solus pour améliorer son intégration.

Il n'y a pas de Gimp 2.9 normal, vu que c'est une version instable. Cela dit, flatpak est dans le dépôt, donc il est possible dans l'avenir d'installer des applications _standalone_ sans bousiller le système.

J'ai le driver NVIDIA 340 alors que j'avais le 381 sous elementary mais via un PPA, ce n'est pas bien grave. Feedreader qui ne fonctionne pas bien sous elementary, fonctionne sans soucis sous Solus car les librairies GNOME sont plus à jour, avantage d'une rolling release.

# Conclusion

Finalement, j'ai retrouvé la totalité des applications que j'utilisais déjà sous elementary OS. Mis à part quelques habitudes prises avec Pantheon qui sont à revoir, le passage se passe plutôt bien. Hâte de voir ce Solus reserve pour l'avenir (passage de Budgie à QT...). Et puis j'ai Virtualbox pour surveiller ce qui se passe sous elementary OS ;-)
