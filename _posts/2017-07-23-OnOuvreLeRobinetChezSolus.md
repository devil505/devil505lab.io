---
title : "On ouvre le robinet chez Solus"
date : "2017-07-23T08:16:00+02:00"
draft : false
thumbnail : "img/solus.jpg"
toc : true # Optional
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---


Le dépôt stable sur Solus a eu droit à sa grosse arrivée de mise à jour. Mis à part la mise à jour du kernel LTS qui est toujours le 4.9 (le 4.12 en unstable présenterait des soucis pour l'instant), l'application de personalisation de Budgie est enfin là !

<img src="../../img/budgie-tweak.jpeg">

Sobre et épurée, y'a juste ce qu'il faut. J'ai adopté les nouveaux thème par défaut de fenêtrage et d'icônes. Pour les polices, Solus utilise par défaut *Noto Sans* et non plus *Clear Sans*.

Du coup, vous pouvez virer gnome-tweak-tool si vous êtes un utilisateur de Budgie Desktop, ca vous fera un paquet inutile (et un raccourci dans le menu) en moins.

Au passage, j'ai récemment trouvé via le journal du hacker, [un bloggeur satisfait de Solus](https://circa1984.net/2017/06/28/jai-teste-solus-os-et-je-lai-adopte/) n'hésitez pas à visiter son blog.
