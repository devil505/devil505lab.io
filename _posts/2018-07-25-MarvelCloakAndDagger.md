---
title : "[Alors on regarde ?] Marvel's Cloak and Dagger"
tags : ["séries TV"]
categories : ["séries TV"]
---

La Cape et l'Épée (Cloak and Dagger en version originale) sont deux personnages de fiction appartenant à l'univers de Marvel Comics. Ils ont été adaptés en série TV (ca devait arriver...). Après, perso, je ne connais pas le comic et apparemment les scénaristes ont pas mal déviés de l'oeuvre originale pour l'adatation série.

Le pitch, deux ados (Tandy,la fille et Tyrone, le garçon) sont plus ou moins connectés par leur passé. En effet, quelques années auparavant, suite à une explosion d'une station pétrolière dans la baie de la Nouvelle Orléans, ils se sont tout les deux, chacun de leur coté, retrouvés dans l'eau et ont été touchés par l'onde de choc.  Les deux personnages sont aussi reliés émotionnellement, Tandy a perdu son père ce soir là et Tyrone son grand-frère. Plusieurs années ont passées et ils se retrouvent un peu par hasard. Cette rencontre va déclencher la découverte de leur propres pouvoirs.

Tandy en touchant quelqu'un peut voir les rêves de la personne qu'elle touche alors que Tyrone c'est l'effet inverse, il peut voir ses peurs. De plus, Tandy peut créer des sortes de crystaux très aguisée comme une dague (Dagger en anglais). Pour sa part, Tyrone est capable de se téléporter et il maitrise bien ce pouvoir, par la suite, s'il porte une sorte de cape (Cloak en anglais).

<iframe width="560" height="315" src="https://www.youtube.com/embed/zW_4nKoIvbw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Il y a deux intrigues en parallèle, chacune réliée à un personnage principal. Tandy veut faire tomber la société pétrolière où travaillait son père, la Roxonn (une entreprise que l'on retrouve dans l'univers Marvel, notamment dans la série _Agent Carter_). Tyrone, lui, veut venger la mort du grand-frère qu'il admirait tant et qui a été tué par un flic ripoux.

<iframe width="560" height="315" src="https://www.youtube.com/embed/5RJnZ1u1WFc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Alors on regarde ? Déjà j'aime bien le lieu de l'action, la Nouvelle Orléans, ca change de New York ou de la Californie. Ce qui est sympa c'est la différence entre les deux personnages. L'une, galère et se met à manipuler les gens pour les voler, l'autre est trop chouchouté par ses parents et est l'un des meilleurs joueurs de l'équipe de Basketball se son école. Malgré leur grandes différences, ils vont devoir s'allier pour se protéger de leurs ennemis. C'est pas du niveau des Marvel par Netflix mais cette série a des éléments intéressants. La saison 2 a été commandée au passage.

