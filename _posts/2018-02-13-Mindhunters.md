---
title : "Mindhunters"
date : "2018-02-13T09:21:00+02:00"
draft : false
thumbnail : "img/seriestv.jpg"
toc : true # Optional
tags : ["séries TV"]
categories : ["séries TV"]
---

Voici une série qui mérite le détour. Mindhunters est une série Netflix produite par David Fincher et CHarlize Theron (du lourd quand même). Pour faire simple, la série relate les début des techniques de profilage des tueurs en série (d'ailleurs ce terme est inventé au cours de la série).

<iframe width="560" height="315" src="https://www.youtube.com/embed/7gZCfRD_zWE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Dans les années 70, deux agents de FBI parcourent les Etats-Unis pour interroger des tueurs en série emprisonnés, ils sont par la suite rejoint par une psychologue. Leur unité va prendre une certaine importance au sein du FBI car son but est de determiner des méthodes de classement de ce type de tueurs et ainsi élucider des crimes similaires.

Les acteurs ne sont pas trop connus mais n'en sont pas moins excellents. L'agent Holden Ford, qui est l'initiateur du projet, est affecté petit à petit par son travail. L'agent Tench, son collègue, est un peu son garde-fou avec son coté très rationnel et qui garde les pieds sur terre.

<img src="https://s14-eu5.ixquick.com/cgi-bin/serveimage?url=https:%2F%2Fcdn.theatlantic.com%2Fassets%2Fmedia%2Fimg%2Fmt%2F2017%2F10%2F391_Mindhunter_103_Unit_04270R2%2Flead_960.jpg%3F1507902613&sp=f8b68a0e4970d468d67e5b56bf3ad680">

Mindhunters c'est un peu du Silence des agneaux et de Zodiac mais avec un coté road trip. Même si l'histoire avance assez lentement et l'action peu présente, c'est surtout l'intensité et la tension des interrogatoires qui tiennent en haleine. La série a été renouvelée pour seconde saison.
