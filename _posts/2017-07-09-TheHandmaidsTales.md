---
title : "The Handmaid's Tales"
date : "2017-07-09T11:24:00+02:00"
draft : false
thumbnail : "img/seriestv.jpg"
toc : true # Optional
tags : ["séries TV"]
categories : ["séries TV"]
---

The Handmaid's Tales c'est la série du moment. Diffusée sur le service de streaming Hulu et tirée d'un livre, elle vaut le détour à mon humble avis.

Alors le pitch, suite à une baisse importante de natalité (stérilité massive ?), la République de Gilead qui est en fait une dictature, a été mise en place aux États-Unis (ou au moins du coté de Boston). Ce nouveau régime, assez religieux au passage, recentre le pouvoir sur les hommes tandis que les femmes passent toute au second plan dans différentes catégories. En effet, les hommes du pouvoir, les commandants, ont leur épouses qui sont les femmes ayant le statut le plus important (et encore...), les Marthas (cuisinières) et les servantes. C'est cette denière catégorie qui est la base de la série. Car le spectateur suit les péripéties d'une des servantes, June rebaptisée "OfFred" (DeFred) car elle est au service du commandant Fred. Elle a tenté de s’enfuir avec son mari et leur fille mais ils ont été rattrapés. Séparée de sa fille, elle se retrouve au Red Center, où on lui inculque avec cruauté comment elle devra se comporter en tant que servante. Assignée à la maison du commandant Fred, elle devra faire certaines tâches comme aller faire les courses mais le plus dur reste la cérémonie. En effet, comme l'épouse du commandant Fred ne peut pas avoir d'enfant, c'est OfFred qui doit coucher avec son mari et ce, en sa présence. Le déroulement de la cérémonie est même expliqué dans le livre de la République, et oui toute bonne dictature a son livre mode d'emploi et celle-ci légalise le viol à sa manière.

Elizabeth Moss endosse à merveille le rôle de OfFred. On a souvent peur pour elle surtout quand elle décide de se rebeller car l'Oeil (service de renseignement de la dictature) est partout. Le premier épisode est même assez dure car avec sa scène de lapidation. Chaque épisode comporte ces moments de flashbacks, où on comprend mieux comment été la vie des personnages autrefois.

La série a marquée l'année 2017 car elle peut faire penser à ce qui ce passe dans sous certains régimes religieux ou encore à la vision de la femme par le nouveau président américain. La saison 2 a été signée.

<iframe width="560" height="315" src="https://www.youtube.com/embed/PJTonrzXTJs" frameborder="0" allowfullscreen></iframe>
