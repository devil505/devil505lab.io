---
title : "brisk menu pour Solus"
date : "2016-12-30T09:01:48+01:00"
draft : false
thumbnail : "img/solus.jpg"
toc : true # Optional
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---

Sur un vieux laptop, j'ai installé Solus dans sa version avec Mate Desktop. C'est léger et fonctionnel. Toutefois, le développeur principal de Solus a développé un menu pour remplacer celui de Mate, il s'agit de Brisk Menu:

<iframe width="560" height="315" src="https://www.youtube.com/embed/U6aRJvtc0cg" frameborder="0" allowfullscreen></iframe>

Le menu est bien organisé mais c'est surtout il n'y a qu'un bouton et pas trois (Applications, Emplacements et Système). Du coup, on gagne de l'espace sur la barre, un atout important pour les petits écrans avec résolution limitée. Il y a aussi une fonction recherche. J'ai assez hâte que ce menu arrive dans le dépôt stable.

Petit bonus, la prochaine version de Budgie Desktop, en développement, devrait être quelque chose, il a même [son propre site](http://budgie-desktop.org/) (encore en construction au moment où j'écris ce billet). On peut présager cela aidera ce nouvel environnement de bureau à être porté sur davantage de distributions linux.
