---
title : "Ma sélection d'applications f-droid pour Août 2018"
tags : ["android"]
categories : ["android"]
---

Voici une petite sélection d'applications que vous trouverez sur le store de [f-droid](https://f-droid.org/).

## Transportr

[transportr](https://f-droid.org/en/packages/de.grobox.liberario/) est l'application pour utiliser les transports en commun. Il a dans sa base de données, plusieurs réseaux du monde entier dont celui de Paris. Le petit plus c'est qu'il n'y a pas de pub, ni de pistage.

## Blokada

J'ai délaissé DNS66 après avoir découvert [blokada](https://f-droid.org/en/packages/org.blokada.alarm). C'est un bloqueur de pub qui ne nécéssite pas d'avoir son téléphone rooté. Tout comme DNS66, on peut forcer le changement des serveurs DNS. Ce que j'aime bien dans blokada c'est que par défaut il y a plusieurs listes de blocage dont celle d'[Adzhosts](https://adzhosts.eu/).

## gloomy dungeon

Voici un jeu opensource façon Doom pour les nostalgiques. Il y a la [version 1](https://f-droid.org/en/packages/zame.GloomyDungeons.opensource.game/), une [version 3D](https://f-droid.org/en/packages/org.zamedev.gloomydungeons1hardcore.opensource/) et la [version 2](https://f-droid.org/en/packages/org.zamedev.gloomydungeons2.opensource/).

## supertuxkart

Si vous êtes sur linux, vous devez surement connaître SuperTuxKart, et bien il y a maintenant [une version mobile](https://f-droid.org/en/packages/org.supertuxkart.stk).

## Ameixa

[Ameixa](https://f-droid.org/en/packages/org.xphnx.ameixa/) est le thème d'icônes que j'utilise, le développeur _thème_ en priorité les applications se trouvant dans f-droid. Il existe aussi une [version monochrome](https://f-droid.org/en/packages/org.xphnx.ameixamonochrome).

On se retrouve le mois prochain pour une nouvelle sélection.

<div id="commento"></div>
<script src="https://cdn.commento.io/js/commento.js"></script>
