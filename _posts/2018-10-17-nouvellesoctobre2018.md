---
title : "Nouvelles d'octobre 2018"
tags : ["linux"]
categories : ["linux"]
---

## Changement de domaine et donc nouvelles images iso chez Solus

Un mal pour un bien. En effet, avec l'absence d'Ikey, le reste de l'équipe de Solus n'a plus la main sur le nom de domaine solus-project.org, du coup, ils dû en prendre un nouveau pour getsol.us. Le hic c'est les liens pour les dépôts stable et unstable. Un neophyte qui installe Solus 3 ne saura pas forcemment [changer l'adresse des dépôts](https://getsol.us/2018/09/11/package-repo-migration-now-available/). Il a donc été décidé de [produire de nouvelles image ISO pour la Solus 3.9999](https://getsol.us/2018/09/20/solus-3-iso-refresh-released/). Perso, ce n'est pas plus mal, car le delta des mise à jour commencait à devenir important avec la Solus 3.

## Gnome 3.30 pas avant Solus 4

C'est Josh qui a décidé pour diverses raisons [ici](https://dev.getsol.us/T6971). J'espère que cela va accélerer la sortie de Solus 4 même s'il reste [des choses à faire](https://dev.getsol.us/T5010) qui, en plus, requiert le retour d'Ikey.

## Solus un peu plus près de sa communauté ?

Solus est assez présent sur le résau social de Google, Google+, mais ce dernier va fermer en été 2019. Cela a fait peur à l'équipe de Solus pour en arriver à pondre [ce billet](https://getsol.us/2018/10/11/improving-community-engagement/). Ils recherchent un nouveau moyen de communication en temps réel selon différents critères (pas de Telegram, ni de Discord, ni de Slack), j'avais proposé mattermost et rocket.chat mais ils ne conviennent pas. C'est franchement pas gagné, ca va etre compliqué de trouver un outil sans faire de concessions. De plus, je trouve IRC franchement depassé (c'est juste un avis perso hein). Le forum de Solus devrait utilisé prochainement [Flarum](https://flarum.org/).

## Elementary 5.0 Juno !

Un mystérieux compteur était apparru sur le site officiel il y a 2 jours, et bien on y est, Juno est [disponible](https://elementary.io/). On verra ce qu'en disent les reviewers. Pas de show vidéo en direct cette fois là pour présenter les nouveautés. Fin de l'effet kiss kool ?

## Peertube et Pixelfed passent en 1.0

Le [Youtube](https://framablog.org/2018/10/15/peertube-1-0-la-plateforme-de-videos-libre-et-federee/) et [l'instagram](https://mastodon.social/@pixelfed/100908785697184005) libres passent en version 1.0 et ca c'est cool !

## Et pendant ce temps dans le libre francophone

On se chamaillent, du moins c'est ce que j'ai aperçu sur Youtube avec la team Tux'n'Vape, Actualia...etc comme l'indique l'ami Seb [sur son billet](http://passiongnulinux.tuxfamily.org/2018/10/08/libriste-fran%C3%A7ais-est-un-con.html). Restez à l'écart de tout cela et profitez de votre distro linux.

Allez, rendez-vous en novembre !

<div id="commento"></div>
<script src="https://cdn.commento.io/js/commento.js"></script>
