---
title : "Nouveautés sur mon Dépôt pour Solus"
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---

Cela fait un moment que je n'avais pas parlé de mon [dépôt tiers pour Solus](https://github.com/Devil505/solus-3rd-party-repo).

J'ai rajouté de nouveaux paquets qui pourraient vous intéresser si vous êtes utilisateurs de Solus.

## harcode-tray

Cette application permet de remplacer certaines icones de la zone notification par des icones plus adéquates, il faut utiliser la commande `sudo hardcode-tray` puis se laisser guider. Ne pas utiliser les thèmes d'icônes Papirus ou Numix durant la manoeuvre sinon ca plante.

Si vous êtes convaincu, laissez un commentaire [ici](https://dev.solus-project.com/D2303) pour que mon patch puisse être approuvé.

## SLSK

SLSK c'est le Steam Linux Swiss Knife, une application en QT pour sauvegarder et restaurer sa config Steam. Une [demande a été faite chez Solus](https://dev.solus-project.com/T5949).

## geth

Une application CLI pour miner de l'ethereum. Je teste par curiosité.

## Applications elementary OS

J'ai packagé quelques applications tierces pour elementary OS qui fonctionnent très bien sous Solus:

 * ciano, un convertisseur audio
 * audience, le lecteur vidéo officiel d'elementary
 * eradio, lecteur de chaines radios
 * notejot, pour afficher une note sur le bureau
 * imageburner, pour mettre une image iso sur support usb
 * playmymucis, un player de biblihotèque audio
 * showmypictures, pour gérer sa collection de photos

## Rappel

Si certaines applications vous plaisent, faîtes une demande chez [Solus](https://dev.solus-project.com/). Si la demande est acceptée, je pourrais ainsi proposer un patch. Par exemple, [Spice-up a été validé](https://dev.solus-project.com/D2386) pour Solus.
