---
title : "Distros Linux pour 2017"
date : "2017-01-14T10:48:51+01:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux","elementary OS"]
categories : ["linux"]
---

Le site [linux.com a publié la liste des distributions linux pour 2017](https://www.linux.com/news/learn/sysadmin/best-linux-distributions-2017) selon des catégories bien définies.

Pour la catégorie distro pour les sysadmins, le gagnant est [Parrot Linux](https://www.parrotsec.org/). Bon, c'est la première fois que j'en entend parler. Cette distro, basée Debian, vient avec par défaut pas mal d'outils d'administration réseau, un peu comme la Kali Linux. Pourquoi pas...

Pour la distro la plus légère, ils ont choisi LXLE. Distro basée Ubuntu LTS avec LXDE. Là vous me direz "et Lubuntu ?", il faut croire que LXLE est plus lègère encore ("surcouche Ubuntu" en moins on vas dire).

Meilleure distro de bureau: elementary OS. Et oui, beaucoup seront surpris, l'an passé c'était Linux Mint. Là aussi pourquoi pas.

La distro des power users: Gentoo. Pas de grande surprise ici.

Meilleures distro pour les serveurs: Centos pour les non-entreprises et Red Hat pour les entreprises. Une fois encore, le classement paraît assez normal pour moi.

Et vous que pensez-vous de ce classement établi par linux.com ?
