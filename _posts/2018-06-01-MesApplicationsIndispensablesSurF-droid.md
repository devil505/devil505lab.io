---
title : "Mes applications indispensables sur F-droid"
tags : ["editos","android"]
categories : ["editos","android"]
---

Voici ma sélection d'applications issues du store f-droid qu'il faut impérativement installer. Cette selection dépend de mon point de vue, n'hésitez pas à me presenter en commentaire d'autres applications qui pourrait valoir le détour :-)

## Fennec

[Fennec](https://f-droid.org/packages/org.mozilla.fennec_fdroid/) est le navigateur web préconisé par f-droid, c'est basé sur Firefox sans les morceaux proprio.

## AndOTP

Si avez Fennec, vous voulez peut-être utiliser Firefox Sync et que si vous avez activé l'authentification à deux facteurs (comme expliquée [ici](https://homputersecurity.com/2018/05/26/comment-activer-lauthentification-a-deux-facteurs-sur-les-comptes-firefox/)), alors l'application [andOTP](https://f-droid.org/packages/org.shadowice.flocke.andotp/) vous sera d'une grande aide pour générer le code de vérification à entrer.

## Firefox Klar

[Firefox Klar](https://f-droid.org/packages/org.mozilla.klar/) est une version de Firefox plus rapide et totalement privée. Ca peut être utile.

## Document Viewer

Pour moi, [Document Viewer](https://f-droid.org/packages/org.sufficientlysecure.viewer/) est, pour moi, le meilleur lecteur de documents PDF, il fait aussi les CBZ, les EPUB et les DjVu.

## Dandelion

[Dandelion](https://f-droid.org/packages/com.github.dfa.diaspora_android/), l'application idéale pour les comptes disapora.

## Tusky

Si vous êtes sur une instance Mastodon, [Tusky](https://f-droid.org/packages/com.keylesspalace.tusky/) est une très bonne application pour cela et en plus il gère le multicomptes.

## Tiny Tiny RSS

Poui suivre mes flux sur framanews, j'utilise [Tiny Tiny RSS](https://f-droid.org/packages/org.fox.tttrss/).

## Nextcloud

Est-il nécéssaire de présenter [Nextcloud](https://f-droid.org/packages/com.nextcloud.client/) ? C'est l'application officielle pour gérer un compte sur un serveur avec nextcloud.

## DAVdroid

Afin de faire synchroniser agenda et carnet d'adresse d'un compte Nextcloud vers Android, y'a pas mieux que [DAVdroid](https://f-droid.org/packages/at.bitfire.davdroid/).

## Telegram

Qu'on aime ou pas [Telegram](https://f-droid.org/packages/org.telegram.messenger/), c'est mon logiciel de messagerie instantanée pour communiquer avec mes contacts linuxiens et/ou utilisateurs de darktable FR.

## SecScanQR

[SecScanQR](https://f-droid.org/packages/de.t_dankworth.secscanqr/) est utile et complet pour les QR codes.

## VLC

Sous android, j'utilisais mxplayer, sur f-droid, il n'existe pas de lecteur vidéo (à ma conaissance). Si vous activez le dépôt archives de f-droid, vous aurez quand même une ancienne version de VLC. Vous pouvez aussi télécharger [ici](https://get.videolan.org/vlc-android/] l'APK officiel et à jour.

## Adaway

[Adaway](https://f-droid.org/packages/org.adaway/) permet de modifier le fichier hosts pour ainsi ne plus jamais être embêté par la publicité. Pour faire fonctionner cette appli, il faudra rooter votre appareil android; donc pensez à télécharger le fichier zip addon-su sur le site de LineageOS et installez le directement via le recovery.

## NewPipe

Oubliez l'application officielle de Youtube, optez pour [NewPipe](https://f-droid.org/packages/org.schabi.newpipe/), plus léger et pourtant avec des options en plus, j'aime bien celle qui permet de faire tourner une vidéo en arrière-plan.
