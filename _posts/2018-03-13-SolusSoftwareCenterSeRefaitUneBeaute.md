---
title : "Le Solus Software Center se refait une beauté"
tags : ["linux", "solus"]
categories : ["linux", "solus"]
---

Le Solus Software Center ou Centre d'Applications de Solus est entrain de subir un redesign pour Solus 4.

## Ce qui a été fait

Ikey a bien bossé, exitla fenêtre de taille carré, on aura droit une fenêtre plus adaptée aux écran larges d'aujourd'hui. L'accent a été mis sur la simplification de la navigation dans les catégories/applications et la découverte de nouvelles applications. Sur ce dernier point, sur la page d'accueil il y aura des applications mise en avant grâce à deux catégories, "Nouveau cette semaine" et "Récemment mis à jour".

<iframe width="560" height="315" src="https://www.youtube.com/embed/AQX5OhAi9bA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Bien sûr cette nouvelle version sera plus rapide au démarrage et plus réactive à l'utilisation.

## Quid des applications tierces

La section _Logiciels tiers_ va disparaître. Via un nouveau système de plugins du Software Center, un plugin sera dédié aux dépôts tiers et on retrouvera les logiciels tiers parmis les catégories standards contenant les paquets du dépôt officiels. Par exemple, Google Chrome dans la catégorie Navigateurs Web, Slack et Skype pour Linux dans la messagerie instantanée, et ainsi de suite. Après la sortie de Solus 4, le Software Center supportera les paquets snaps. L'utilisateur pourra visuellement différencier les paquets officiels et les logiciels tiers.

## Ce qui reste à faire

Il reste encore des améliorations visuelles à réaliser comme un design plus _responsive_ suivant les différentes résolutions d'écran. Des efforts sont aussi à apportées pour la rapidité comme réduire le temps de démarrage du Software Center ou encore placer les opération d'installation ou désinstallation de paquets dans une file d'attente dédiée.

Github du projet: [https://github.com/solus-project/solus-sc](https://github.com/solus-project/solus-sc)



 
