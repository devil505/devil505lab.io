---
title : "Nouvelles de Juin 2018"
tags : ["linux"]
categories : ["linux"]
---

Et oui, le blog change d'adresse et se trouve désormais sur gitlab. Pas besoin de vous expliquer le pourquoi, on en parle déjà un peu partout :-D

## Solus 3.9999

Ikey joue encore avec nos nerfs en mettant le numéro de release à 3.9999 après la dernière mise à jour sur Solus. Du coup, certains en profitent pour se moquer gentillement:

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">I come from the future to bring you some <a href="https://twitter.com/SolusProject?ref_src=twsrc%5Etfw">@SolusProject</a> news <a href="https://t.co/MxCbcrSkRe">pic.twitter.com/MxCbcrSkRe</a></p>&mdash; Peter O&#39;Connor (@sunnyflunk1) <a href="https://twitter.com/sunnyflunk1/status/1001130120420065282?ref_src=twsrc%5Etfw">28 mai 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Ikey attend surement de terminer le boulot sur la refonte du Solus software center, et durant le mois de mai, il faut avouer qu'il a bien bossé:

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">Progress bits are now hooked up in the upcoming new Software Center&#39;s sidebar, with remove/install fully implemented, defaulting to automatic package removal. i.e. removing 0ad will actually remove [&#39;0ad&#39;, &#39;gloox&#39;, &#39;miniupnpc&#39;, &#39;0ad-data&#39;, &#39;enet&#39;, &#39;libsodium&#39;]. <a href="https://twitter.com/hashtag/Solus?src=hash&amp;ref_src=twsrc%5Etfw">#Solus</a> <a href="https://t.co/Q7r5LHwNbA">pic.twitter.com/Q7r5LHwNbA</a></p>&mdash; Solus (@SolusProject) <a href="https://twitter.com/SolusProject/status/997914031162822656?ref_src=twsrc%5Etfw">19 mai 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">Upcoming new Software Center will now pop up a notification once a job has completed. <a href="https://twitter.com/hashtag/Solus?src=hash&amp;ref_src=twsrc%5Etfw">#Solus</a> <a href="https://t.co/qz31SfQkU4">pic.twitter.com/qz31SfQkU4</a></p>&mdash; Solus (@SolusProject) <a href="https://twitter.com/SolusProject/status/997915056653393920?ref_src=twsrc%5Etfw">19 mai 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Les mises à jour vont bon train comme d'hab, et, au passage, petit victoire personnelle, [mon paquet frogr a été approuvé](https://dev.solus-project.com/D2635).

<img src="../../img/keepcalmsolus.jpg">

## Pendant ce temps chez elementary OS...

Daniel Foré fais du teasing en promettant que le passage de Loki à Juno sera [un coup de jeune](https://plus.google.com/b/109348840800096254191/+DanielFor%C3%A9/posts/YiPnFbpDtin). Il bosse aussi sur [un bouton pour mode jour/nuit](https://plus.google.com/b/109348840800096254191/+DanielFor%C3%A9/posts/bK2pFp5hjv2) sur les fenêtres. Les projets utilisent [Meson à la place de CMake](https://github.com/orgs/elementary/projects/5#card-8566658). Aussi, Daniel [a bien bossé sur le thème d'icônes](https://plus.google.com/b/109348840800096254191/+DanielFor%C3%A9/posts/U4Zw3RLVfLK), Juno s'annonce superbe à mon avis.

### Un peu plus de libre

Une fois n'est pas coutume, une petite news sur le libre pour vous annoncer qu'après l'alternative à YouTube, PeerTube, nous avons des alternatives libres à Spotify et Instagram. En effet, FunkWhale peut remplacer Spotify and co, encore que, avec les problèmes de droits d'auteur ça va pas être simple. DanSup a officiellement lancé la bêta de [PixelFed](https://pixelfed.social/), un clone libre d'instagram, même si les chances que cela risque d'être à terme une niche à geeks sont fortes (je vois mal Kim Kardashian passé à PixelFed). Toutefois, on ne peut que saluer l'initiative.

Rendez-vous en juillet !

