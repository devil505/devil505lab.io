---
title : "Nouvelles de Janvier 2018"
date : "2018-01-16T19:08:00+02:00"
draft : false
thumbnail : "img/linux.jpg"
toc : true # Optional
tags : ["linux"]
categories : ["linux"]
---

La nouvelle année commence doucement chez Solus. Solus 4 n'est pas encore sorti. Ikey s'amuse un peu avec Budgie 11 avec QT et Wayland d'où le petit retard...

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">Some experimentation from the holidays - working on a QtWayland based Wayland compositor for <a href="https://twitter.com/hashtag/Budgie?src=hash&amp;ref_src=twsrc%5Etfw">#Budgie</a> 11. Demonstrated here on the proprietary NVIDIA drivers (using eglstreams). Note this implementation is pure Qt/C++/OpenGL. <a href="https://twitter.com/hashtag/Solus?src=hash&amp;ref_src=twsrc%5Etfw">#Solus</a><a href="https://t.co/xNcEFiCQRQ">https://t.co/xNcEFiCQRQ</a></p>&mdash; Solus (@SolusProject) <a href="https://twitter.com/SolusProject/status/948998449248325638?ref_src=twsrc%5Etfw">4 janvier 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Ikey a ensuite pris une semaine de vacances et entre nous c'est quand même bien mérité. Là, d'après Kyrios, l'équipe s'active pour sortir Solus 4 avec un software center tout beau qui aura le support pour des paquets tiers utilisant snap.

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">I&#39;m uh.. on .. vacation? This is a strange thing. A whole week, even. <a href="https://t.co/AEI5APx8CO">pic.twitter.com/AEI5APx8CO</a></p>&mdash; Ikey Doherty (@ufee1dead) <a href="https://twitter.com/ufee1dead/status/950497488368226304?ref_src=twsrc%5Etfw">8 janvier 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Sachez également que Ikey dispose de nouveaux ordinateurs en plus et la particuliarité c'est que ce ne sont pas des bêtes de course, ideal pour des tests car ces machines se rapprochent de celles des utilisateurs standards.

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">Proud new owner of a 2nd hand craptop! Got myself an entry level system to help with <a href="https://twitter.com/SolusProject?ref_src=twsrc%5Etfw">@SolusProject</a> development, so that I get a real feel for how stuff runs for normal users, and improve it. 4GB RAM, 750GB HDD, i5-3230M. A beast! <a href="https://t.co/C3iF1CGOFV">pic.twitter.com/C3iF1CGOFV</a></p>&mdash; Ikey Doherty (@ufee1dead) <a href="https://twitter.com/ufee1dead/status/949400643076263936?ref_src=twsrc%5Etfw">5 janvier 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


Coté elementary ca bosse toujours sur l'installateur et le visuel. Apparemment Juno devrait sortir vers Avril-Mai 2018, idem pour Pop OS 18.04, la distro de System76.


<img src="https://lh3.googleusercontent.com/-GGV2xFx_Is8/WlDR9kIuTkI/AAAAAAAAyks/k6tvNJ7xGk4VbIq7yKbO5P9NrlkZvB3PACJoC/w530-h298-n/Screenshot%2Bfrom%2B2017-11-15%2B14.41.17.png">

L'éditeur Scratch a été rebasptisé elementary-code.

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">Our very first sneak peak at one of the changes coming in Juno is here! <a href="https://t.co/dthRcRDaMh">https://t.co/dthRcRDaMh</a></p>&mdash; elementary (@elementary) <a href="https://twitter.com/elementary/status/948340881958072320?ref_src=twsrc%5Etfw">2 janvier 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Voilà, on se retrouve en février ;-)





