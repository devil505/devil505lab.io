---
title : "Nouvelles d'Avril 2018"
tags : ["linux"]
categories : ["linux"]
---

## elementary OS

L'équipe a publié les news de Mars concernant Juno. J'ai trouvé une vidéo sur Youtube qui résume bien tout ca:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Spd4Nnjj6RY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

La fenêtre de rappel des raccoucis semble bien pratique, surtout que Pantheon en dispose de vraiment intéressants. Il sera donc possible de configurer la touche Super (celle avec le logo Windows sur beaucoupd de claviers...). Le plugin switchboard qui gère la vie privée aura en plus la fonction géolocalisation. Et on voit bien que l'éditeur de texte scracth-text-editor qui devient elementary-code se destine à devenir un éditeur pour développeur avec un tas de fonctions. Cela s'annonce bien pour Juno, qui sortira selon mes estimations en septembre 2018.

Sur Github, l'équipe bosse pas mal sur l'installateur, surement poussé par System76 qui, il me semble, l'a déjà mis en place sur la récente version beta de Pop! OS. C'est bien un installateur OEM car de ce que j'ai vu sur certaines vidéos, la partie configuration de l'utilisateur et celle du clavier s'effectue au reboot après installation. Pop! OS est une Ubuntu 18.04 revampée avec un thème maison et utilisant l'installateur d'elementary. Quitte à travailler avec la team elementary pourquoi ne pas proposer elementary OS directement...enfin bref. À part ca l'installateur d'elementary OS semble vraiment bien foutu, claire et épuré:

<iframe width="560" height="315" src="https://www.youtube.com/embed/N26sLaIvoNA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Solus

Pas grand chose chez Solus, la vitesse de développement s'est un peu ralenti. Ikey a des activités qui lui prend un peu de temps. Néanmoins, il a repris le développement du Solus Software Center qui dispose d'une [queue list latérale](https://plus.google.com/b/109348840800096254191/+Solus-Project/posts/Ya9CwUzwXnr) et, de plus, l'application s'intègre également [dans un environnement Plasma](https://plus.google.com/b/109348840800096254191/+Solus-Project/posts/36BGNr4f3vD).

Malgré une panne du serveur chez OVH qui a duré une journée, le service est assuré avec l'habituelle synchonisation du vendredi. Dernièrement, mise à jour du kernel en 4.15.15, de Mesa et de quelques applications comme Calibre.

La solus 4 se fait toujours attendre, et Ikey, par humour a mis à jour le numéro de release à 3.99 puis 3.999.

Allez, au mois de Mai !


